package com.example.pclassmanagement.ui.leader_board

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.data.model.UserContributionData
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.item_leader_board.view.*

class LeaderBoardAdapter constructor(
    private val actionDispatcher: ActionDispatcher,
    private val data: List<UserContributionData>
) :
    RecyclerView.Adapter<LeaderBoardAdapter.CommentViewHolder>() {

    private var userRef = FirebaseDatabase.getInstance().getReference(Table.USER)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            actionDispatcher,
            LayoutInflater.from(parent.context).inflate(R.layout.item_leader_board, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindView(data[position], position)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class CommentViewHolder(private val actionDispatcher: ActionDispatcher, itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindView(item: UserContributionData, index: Int) {
            itemView.tv_point_lb.text = item.totalPoint.toString()
            itemView.tv_order_num_lb.text = (index + 1).toString() +  ". "

            // get name, code of user by userId
            userRef.child(item.userContribution.userId).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)
                        if (user != null) {
                            if (user.imageUrl.isNotEmpty()) {
                                Glide.with(itemView.context)
                                    .load(user.imageUrl)
                                    .into(itemView.iv_user_avatar_lb)
                            }
                            itemView.tv_full_name_lb.text = user.fullname
                            itemView.tv_username_lb.text = user.studentCode
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                }
            )

//            itemView.bt_comment.setOnClickListener {
//                actionDispatcher.dispatch(
//                    ActionWrapper(
//                        name = Action.Item.COMMENT_POST,
//                        payload = item
//                    )
//                )
//            }
        }
    }

}

