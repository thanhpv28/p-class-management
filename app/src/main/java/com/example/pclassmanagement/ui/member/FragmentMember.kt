package com.example.pclassmanagement.ui.member

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.*
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.data.model.UserClass
import com.example.pclassmanagement.databinding.FragmentMemberBinding
import com.example.pclassmanagement.navigation.Navigator.requestNavigate
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseFragment
import com.example.pclassmanagement.ui.chat.ActivityMessage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class FragmentMember : BaseFragment() {

    private lateinit var binding: FragmentMemberBinding
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val userRef = FirebaseDatabase.getInstance().getReference(Table.USER)
    private val userClassRef = FirebaseDatabase.getInstance().getReference(Table.USER_CLASS)
    private var memberList = mutableListOf<User>()

    var classId = ""

    private lateinit var memberAdapter: MemberAdapter

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            when(actionWrapper?.name){
                Action.Item.VIEW_PROFILE -> {
                    val user = actionWrapper.payload as User
                    requestNavigate(R.id.fragmentProfile, bundleOf(
                        FragmentProfileARGS.KEY.USER_ID to user.id
                    ))
                }
                Action.Item.MESSAGE -> {
                    val user = actionWrapper.payload as User
                    startActivity(Intent(context, ActivityMessage::class.java).apply {
                        putExtra(FragmentChatARGS.KEY.USER_ID , user.id)
                        putExtra(FragmentChatARGS.KEY.IS_SHOW_MESSAGE_DETAIL , true)
                    })
//                    requestNavigate(R.id.fragmentChat, bundleOf(
//                        FragmentChatARGS.KEY.USER_ID to user.id,
//                        FragmentChatARGS.KEY.IS_SHOW_MESSAGE_DETAIL to true
//                    ))
                }
            }


        }
    })


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMemberBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initData() {
        classId = arguments?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        super.initData()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun initView() {
        super.initView()
        memberAdapter = MemberAdapter(actionDispatcher, memberList)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvMember.layoutManager = layoutManager
        binding.rvMember.adapter = memberAdapter
        memberList.clear()

        userClassRef.orderByChild("classId").equalTo(classId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach { uc ->
                        val userClass = uc.getValue(UserClass::class.java)

                        //getUserById
                        userRef.child(userClass?.userId.toString()).get()
                            .addOnCompleteListener { u ->
                                val user = u.result.getValue(User::class.java)
                                if (user != null) {
                                    memberList.add(user)
                                }
                                memberAdapter.notifyDataSetChanged()
                            }
                    }

                }

                override fun onCancelled(error: DatabaseError) {

                }

            }
            )
    }

    override fun initListener() {
        super.initListener()
        binding.btAddMember.setOnClickListener {
            requestNavigate(id = R.id.fragmentAddMember, bundleOf(
                FragmentLandingPageARGS.KEY.CLASS_ID to classId
            ))
        }
    }


}