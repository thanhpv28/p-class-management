package com.example.pclassmanagement.ui.class_detail

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.ContributionPoint
import com.example.pclassmanagement.data.ContributionType
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.StorageRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ClassViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val contentRepository: ContentRepository,
    private val storageRepository: StorageRepository,
) : ViewModel() {

    lateinit var postData: Post

    private val _classDetail =
        MutableSharedFlow<ResultWrapper<ClassData>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val classDetail: SharedFlow<ResultWrapper<ClassData>>
        get() = _classDetail

    private val _ownerInfo =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val ownerInfo: SharedFlow<ResultWrapper<User>>
        get() = _ownerInfo

    private val _currentUser =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val currentUser: SharedFlow<ResultWrapper<User>>
        get() = _currentUser

    private val _uploadImageStatus =
        MutableSharedFlow<ResultWrapper<String>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val uploadImageStatus: SharedFlow<ResultWrapper<String>>
        get() = _uploadImageStatus

    fun getClassDetail(classId: String) {
        viewModelScope.launch {
            contentRepository.getClassDetail(classId).collect {
                _classDetail.tryEmit(it)
            }
        }
    }

    private val _createPostStatus =
        MutableSharedFlow<ResultWrapper<Boolean>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val createPostStatus: SharedFlow<ResultWrapper<Boolean>>
        get() = _createPostStatus

    fun getOwnerInfo(ownerId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(ownerId).collect {
                _ownerInfo.tryEmit(it)
            }
        }
    }

    fun getUserInfo(userId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(userId).collect {
                _currentUser.tryEmit(it)
            }
        }
    }

    fun uploadPostImage(uri: Uri) {
        viewModelScope.launch {
            storageRepository.uploadPostImage(postData.id, uri).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    postData.thumb = it.data.toString()
                    _uploadImageStatus.tryEmit(it)
                }
            }
        }
    }

    fun createPost() {
        viewModelScope.launch {
            contentRepository.createPost(postData).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    _createPostStatus.tryEmit(it)
                    contentRepository.addContribution(
                        classId = postData.classId,
                        type = ContributionType.CREATE_POST,
                        point = ContributionPoint.CREATE_POST
                    )
                }
            }
        }
    }

    fun addComment(postId: String, content: String, classId: String) {
        viewModelScope.launch {
            contentRepository.addComment(postId, content).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    contentRepository.addContribution(
                        classId = classId,
                        type = ContributionType.COMMENT,
                        point = ContributionPoint.COMMENT
                    )
                }
            }
        }
    }

    private val _userContributionList =
        MutableSharedFlow<List<UserContributionData>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val userContributionList: SharedFlow<List<UserContributionData>>
        get() = _userContributionList

    fun getClassContributionLeaderboard(classId: String) {
        viewModelScope.launch {
            contentRepository.getAllContributionByClassId(classId).collect {

                if (it.status == ResultWrapper.Status.SUCCESS) {
                    val userContributionDataList = mutableListOf<UserContributionData>()
                    it.data?.let { ucList ->
                        var totalPoint = 0L
                        ucList.forEach { uc ->
                            uc.contributionList.forEach { value ->
                                totalPoint += value.point
                            }
                            userContributionDataList.add(UserContributionData(
                                userCode = "B18",
                                userContribution = uc,
                                totalPoint = totalPoint,
                                userFullName = "Fullname"
                            ))
                        }
                        _userContributionList.tryEmit(userContributionDataList)
                    }
                }
            }
        }
    }

    private val _likePostSuccess =
        MutableSharedFlow<Boolean>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val likePostSuccess: SharedFlow<Boolean>
        get() = _likePostSuccess

    fun likePost(postId: String) {
        viewModelScope.launch {
            contentRepository.addLike(postId).collectLatest {
                if(it.status == ResultWrapper.Status.SUCCESS){
                    it.data?.let { it1 -> _likePostSuccess.tryEmit(it1) }
                }
            }

        }

//            sharedViewModel.addContribution(
//                classId = classId,
//                type = ContributionType.LIKE,
//                point = ContributionPoint.LIKE
//            )

    }
}