package com.example.pclassmanagement.ui.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.local.SharedPreferencesHelper
import com.example.pclassmanagement.data.model.UserInfoSaved
import com.example.pclassmanagement.data.repository.UserRepository
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val sharedPreferencesHelper: SharedPreferencesHelper,
) : ViewModel() {

    private val _loginState =
        MutableSharedFlow<ResultWrapper<AuthResult>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val loginState: SharedFlow<ResultWrapper<AuthResult>>
        get() = _loginState
    private val _registerState =
        MutableSharedFlow<ResultWrapper<AuthResult>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )
    val registerState: SharedFlow<ResultWrapper<AuthResult>>
        get() = _registerState

    fun getUserSaved(): UserInfoSaved {
        return sharedPreferencesHelper.getUserInfo()
    }

    fun login(email: String, password: String, remember: Boolean) {
        viewModelScope.launch {
            userRepository.signInWithEmailAndPassword(email, password).collect {
                _loginState.tryEmit(it)
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    if (remember)
                        sharedPreferencesHelper.saveInfo(
                            userId = FirebaseAuth.getInstance().uid.toString(),
                            email = email,
                            password = password,
                            remember = true
                        )
                }
            }
        }
    }

    fun register(username: String, email: String, password: String) {
        viewModelScope.launch {
            userRepository.register(username, email, password).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    val firebaseUser: FirebaseUser = it.data?.user!!
                    val userId = firebaseUser.uid
                    val mRef = FirebaseDatabase.getInstance()
                        .getReference("User").child(userId)
                    val hashMap = HashMap<String, String>()
                    hashMap["id"] = userId
                    hashMap["username"] = username
                    hashMap["email"] = email
                    hashMap["password"] = password
                    hashMap["imageUrl"] = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png"
                    hashMap["background"] = ""
                    hashMap["fullname"] = username
                    val hashMap1 = HashMap<String, Int>()
                    hashMap1["avatarId"] = 0
                    hashMap1["backgroundId"] = 0
                    mRef.setValue(hashMap)
                }

                _registerState.tryEmit(it)
            }
        }
    }
}
