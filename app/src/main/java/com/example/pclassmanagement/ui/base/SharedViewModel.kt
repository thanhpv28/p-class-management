package com.example.pclassmanagement.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.repository.ContentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(
    private val contentRepository: ContentRepository,
) : ViewModel() {

    fun addContribution(userId: String = "", classId: String, type: Long, point: Long) {
        viewModelScope.launch {
            contentRepository.addContribution(userId, classId, type, point).collectLatest {
            }
        }
    }

}