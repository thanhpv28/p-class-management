package com.example.pclassmanagement.ui.class_lesson

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.pclassmanagement.data.FragmentLessonARGS
import com.example.pclassmanagement.data.model.Lesson
import com.example.pclassmanagement.data.model.LessonFeedBack
import com.example.pclassmanagement.databinding.FragmentLessonFeedbackBinding
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class FragmentLessonFeedBack : DialogFragment() {

    private lateinit var binding: FragmentLessonFeedbackBinding
    private val lessonViewModel: LessonViewModel by viewModels()
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private var lessonId = ""
    private var classId = ""
    private lateinit var lesson: Lesson


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lessonId = arguments?.getString(FragmentLessonARGS.KEY.LESSON_ID).toString()
        classId = arguments?.getString(FragmentLessonARGS.KEY.CLASS_ID).toString()
        lesson = arguments?.getSerializable(FragmentLessonARGS.KEY.LESSON) as Lesson
//        lessonViewModel.
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentLessonFeedbackBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initListener() {
        lifecycleScope.launch {
            lessonViewModel.feedbackStatus.collect {
                if(it){
                    Toast.makeText(context, "Gửi đánh giá thành công!",Toast.LENGTH_SHORT).show()
                    dismiss()
                }
            }
        }

        binding.btSendFeedback.setOnClickListener {
            if(binding.editText.text.toString().trim().isEmpty())
                return@setOnClickListener
            val lessonFeedBack = LessonFeedBack(
                userId = if (binding.switchAnonymous.isChecked) "" else firebaseUser?.uid ?: "",
                lessonId,
                rating = binding.appCompatRatingBar.rating,
                message = binding.editText.text.toString().trim()
            )
            lessonViewModel.sendFeedBack(lessonFeedBack, classId)
        }
    }

}