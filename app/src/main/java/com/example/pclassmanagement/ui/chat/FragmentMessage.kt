package com.example.pclassmanagement.ui.chat

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.*
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.databinding.FragmentMessageBinding
import com.example.pclassmanagement.navigation.Navigator.popBack
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseActivity
import com.example.pclassmanagement.ui.base.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ActivityMessage : BaseActivity() {

    private lateinit var binding: FragmentMessageBinding
    private val uid = FirebaseAuth.getInstance().currentUser?.uid
    private val messageRef = FirebaseDatabase.getInstance().getReference(Table.MESSAGE)
    private val chatRef = FirebaseDatabase.getInstance().getReference(Table.CHAT)
    private val userRef = FirebaseDatabase.getInstance().getReference(Table.USER)

    private var messageList = mutableListOf<Message>()
    var uid2 = ""
    var chatId = ""

    private lateinit var messageAdapter: MessageAdapter

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val assignment = actionWrapper?.payload as Assignment

        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_message)
        super.onCreate(savedInstanceState)
    }

    override fun initData() {
        super.initData()
        uid2 = intent.getStringExtra(FragmentChatARGS.KEY.USER_ID) ?: ""
        chatId = intent.getStringExtra(FragmentChatARGS.KEY.CHAT_ID) ?: ""
    }

    override fun initView() {
        super.initView()
        messageAdapter = MessageAdapter(actionDispatcher, messageList)
        val layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        binding.rvMessage.layoutManager = layoutManager
        binding.rvMessage.adapter = messageAdapter
        messageList.clear()
        chatRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.childrenCount == 0L)
                    binding.tvNoMessage.show()
                else
                    binding.tvNoMessage.gone()
                snapshot.children.forEach {
                    val chat = it.getValue(Chat::class.java)
                    if (chat != null) {
                        if (chat.uid1 == uid && chat.uid2 == uid2 ||
                            chat.uid2 == uid && chat.uid1 == uid2
                        ) {
                            chatId = chat.id
                            messageRef.orderByChild("chatId").equalTo(chatId)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onDataChange(snapshot: DataSnapshot) {
                                        log(snapshot.toString())
                                        val oldSize = messageList.size
                                        messageList.clear()
                                        snapshot.children.forEach { snapshot1 ->
                                            val message = snapshot1.getValue(Message::class.java)
                                            if (message != null) {
                                                messageList.add(message)
                                            }
                                            messageAdapter.notifyItemRangeInserted(
                                                oldSize,
                                                messageList.size - oldSize
                                            )
                                            binding.rvMessage.smoothScrollToPosition(messageList.size - 1)
                                        }
                                    }

                                    override fun onCancelled(error: DatabaseError) {

                                    }
                                })


                        }

                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

        userRef.child(uid2).get().addOnCompleteListener {
            if(it.isSuccessful){
                val user2 = it.result.getValue(User::class.java)
                binding.tvUserName.text = user2?.username
                if(user2?.imageUrl?.isNotEmpty() == true){
                    Glide.with(baseContext)
                        .load(user2.imageUrl)
                        .into(binding.ivUser)
                }
            }
        }


    }

    override fun initListener() {
        super.initListener()
        binding.btnSend.setOnClickListener {
            val msg = binding.etInputMessage.text.toString()
            if(msg.isNotEmpty()){
                binding.etInputMessage.text = Editable.Factory.getInstance().newEditable("")
                if(chatId.isEmpty()){
                    chatId = chatRef.push().key.toString()
                    chatRef.child(chatId).setValue(Chat(chatId, uid.toString(), uid2, msg, System.currentTimeMillis())).addOnCompleteListener {
                        val message = Message(msg, chatId, uid.toString(), System.currentTimeMillis())
                        messageRef.push().setValue(message)
                    }
                }
                else{
                    chatRef.child(chatId).child("lastMessage").setValue(msg).addOnCompleteListener {

                    }
                    val message = Message(msg, chatId, uid.toString(), System.currentTimeMillis())
                    messageRef.push().setValue(message)
                }

            }
        }

        binding.btnBack.setOnClickListener {
            finish()
        }
    }


}