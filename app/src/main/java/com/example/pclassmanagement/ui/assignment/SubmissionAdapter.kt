package com.example.pclassmanagement.ui.assignment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Mark
import com.example.pclassmanagement.data.model.Submission
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.item_submission.view.*

class SubmissionAdapter constructor(
    private val actionDispatcher: ActionDispatcher,
    private val data: List<Submission>,
    private val context: Context,
) :
    RecyclerView.Adapter<SubmissionAdapter.SubmissionViewHolder>() {

    private val userRef =
        FirebaseDatabase.getInstance().getReference(Table.USER)

    private val markRef =
        FirebaseDatabase.getInstance().getReference(Table.MARK)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubmissionViewHolder {
        return SubmissionViewHolder(
            actionDispatcher,
            LayoutInflater.from(parent.context).inflate(R.layout.item_submission, parent, false)
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: SubmissionViewHolder, position: Int) {
        holder.bindView(data[position], context)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class SubmissionViewHolder(
        private val actionDispatcher: ActionDispatcher,
        itemView: View
    ) :
        RecyclerView.ViewHolder(itemView) {
        @RequiresApi(Build.VERSION_CODES.O)
        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        fun bindView(item: Submission, context: Context) {
            itemView.tv_item_file_name.text = item.fileName
            itemView.tv_item_submitted_date.text = Utils.getDateFromMillisecond(item.createdDate)
            if (item.late) {
                itemView.tv_item_status.text = "Nộp muộn"
                itemView.tv_item_status.setTextColor(
                    context.resources.getColor(
                        R.color.color_error,
                        context.theme
                    )
                )
                itemView.item_bg.setBackgroundColor(
                    context.resources.getColor(
                        R.color.color_error_bg,
                        context.theme
                    )
                )
            }
//             who is the commenter
            userRef.child(item.userId)
                .addListenerForSingleValueEvent(
                    object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val user = snapshot.getValue(User::class.java)
                            if (user != null) {
                                if (user.imageUrl.isNotEmpty()) {
                                    Glide.with(itemView.context)
                                        .load(user.imageUrl)
                                        .into(itemView.iv_item_avatar)
                                }
                                itemView.tv_item_full_name.text = user.fullname + " -"
                                itemView.tv_item_msv.text = user.studentCode
                            }


                        }

                        override fun onCancelled(error: DatabaseError) {

                        }

                    }
                )

            markRef.child(item.assignmentId).child(item.userId)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val mark = snapshot.getValue(Mark::class.java)
                        if(mark != null){
                            itemView.group_mark.show()
                            itemView.tv_item_mark.text = "Đã chấm điểm: " + mark.mark
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })



            itemView.setOnClickListener {
                actionDispatcher.dispatch(actionWrapper = ActionWrapper(payload = item))
            }


        }
    }

}

