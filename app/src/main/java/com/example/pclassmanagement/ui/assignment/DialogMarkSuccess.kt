package com.example.pclassmanagement.ui.assignment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.pclassmanagement.R
import kotlinx.android.synthetic.main.fragment_create_class.bt_cancel

class DialogMarkSuccess(val score: Double) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_mark_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_cancel.setOnClickListener {
            dismiss()
        }
        view.findViewById<TextView>(R.id.tv_score).text =
            "Bạn đã hoàn thành bài thi với số điểm $score"
    }


}