package com.example.pclassmanagement.ui.class_detail

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.Action
import com.example.pclassmanagement.data.FragmentHomeARGS
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Post
import com.example.pclassmanagement.databinding.FragmentHomeBinding
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseFragment
import com.example.pclassmanagement.ui.comment.FragmentComment
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FragmentHome : BaseFragment() {

    private val classViewModel: ClassViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding

    private val userClassRef = FirebaseDatabase.getInstance().getReference(Table.USER_CLASS)
    private val postRef = FirebaseDatabase.getInstance().getReference(Table.POST)
    private val likeRef = FirebaseDatabase.getInstance().getReference(Table.LIKE)
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private var postList = mutableListOf<Post>()
    var classId = ""
    var ownerId = ""

    //firebase Storage
    private val storageRef = FirebaseStorage.getInstance()
    private var selectedFile: Uri? = null


    private lateinit var postAdapter: PostAdapter

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val post = actionWrapper?.payload as Post
            when (actionWrapper.name) {
                Action.Item.LIKE_POST -> {
                }
                Action.Item.COMMENT_POST -> {
                    val dialog = FragmentComment()
                    dialog.arguments = bundleOf(
                        FragmentHomeARGS.KEY.POST to post
                    )
                    dialog.show(parentFragmentManager, null)
                }
            }
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun initData() {
        classId = arguments?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        ownerId = arguments?.getString(FragmentLandingPageARGS.KEY.OWNER_ID).toString()
        super.initData()
    }

    override fun initView() {
        super.initView()
        showLoading()
        // init class detail
        classViewModel.getClassDetail(classId)
        classViewModel.getOwnerInfo(ownerId)
        //if you are not the owner, get your info to do action on this class as a member
        if (ownerId != firebaseUser?.uid) classViewModel.getUserInfo(firebaseUser?.uid.toString())
        postList.clear()
        postAdapter = PostAdapter(actionDispatcher, postList, sharedViewModel)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
        binding.rvPost.layoutManager = layoutManager
        binding.rvPost.adapter = postAdapter

        postRef.child(classId)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val oldSize = postList.size
                    postList.clear()
                    snapshot.children.forEach {
                        val post = it.getValue(Post::class.java)
                        if (post != null) {
                            postList.add(post)

                        }
                    }
                    postAdapter.notifyItemRangeInserted(oldSize, postList.size - oldSize)
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
        userClassRef.orderByChild("classId").equalTo(classId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    binding.tvMemNumber.text = snapshot.childrenCount.toString() + " thành viên"
                }

                override fun onCancelled(error: DatabaseError) {
                }

            }
            )

    }

    override fun initListener() {
        super.initListener()
        binding.btPost.setOnClickListener {
            val content = binding.etPostStatus.text.toString()
            if (content.isEmpty() && selectedFile == null) {
                showToast("Bạn cần điền nội dung bài đăng!")
            } else {
                showLoading()
                val postId = Utils.createId()
                val ownerId = firebaseUser?.uid.toString()
                val type = 0L
                val thumb = ""
                val createdDate = System.currentTimeMillis()

                val postData = Post(postId, ownerId, classId, type, thumb, content, createdDate)
                classViewModel.postData = postData
                submitFile()

            }
        }

        binding.ivImagePick.setOnClickListener {
            ImagePicker.with(this)
                .galleryOnly()
                .start()
        }

        binding.ivFilePick.setOnClickListener {

        }

        // listen class detail
        lifecycleScope.launch {
            classViewModel.classDetail.collect {
                hideLoading()
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    binding.tvGroupName.text = it.data?.name ?: ""
                    // set class background
                    if (this@FragmentHome.isVisible)
                        Glide.with(requireContext())
                            .load(it.data?.thumb)
                            .placeholder(R.drawable.placeholder)
                            .into(binding.ivClassThumb)

                }
            }
        }

        // listen owner info
        lifecycleScope.launch {
            classViewModel.ownerInfo.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    binding.tvClassOwner.text = it.data?.fullname ?: ""
                    if (ownerId == firebaseUser?.uid) {
                        binding.tvClassOwner.text = it.data?.fullname ?: ""
                        Glide.with(requireContext())
                            .load(it.data?.imageUrl)
                            .placeholder(R.drawable.placeholder)
                            .into(binding.ivUser)
                    }
                }
            }
        }

        // listen current info
        lifecycleScope.launch {
            classViewModel.currentUser.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    Glide.with(requireContext())
                        .load(it.data?.imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(binding.ivUser)
                }
            }
        }

        // listen upload image status
        lifecycleScope.launch {
            classViewModel.uploadImageStatus.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    classViewModel.createPost()
                }
            }
        }

        // listen create post
        lifecycleScope.launch {
            classViewModel.createPostStatus.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    showToast("Đăng bài thành công!")
                    binding.etPostStatus.text =
                        Editable.Factory.getInstance().newEditable("")
                    hideLoading()
                    binding.ivImagePost.gone()
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
            Log.d("selected", selectedFile.toString())

        }
        when (resultCode) {
            Activity.RESULT_OK -> {
                val uri: Uri = data?.data!!
                selectedFile = uri
                binding.ivImagePost.setImageURI(selectedFile)
                binding.ivImagePost.show()
                // Use Uri object instead of File to avoid storage permissions
                //            imgProfile.setImageURI(fileUri)
            }
            ImagePicker.RESULT_ERROR -> {
            }
            else -> {
            }
        }
    }

    private fun submitFile() {
        if (selectedFile != null) {
            classViewModel.uploadPostImage(selectedFile!!)
            selectedFile = null
        } else {
            classViewModel.createPost()
//            postRef.push().setValue(classViewModel.postData).addOnCompleteListener {
//                if (it.isSuccessful) {
//                    showToast("Your status is posted successfully!")
//                    binding.etPostStatus.text = Editable.Factory.getInstance().newEditable("")
//
//                    hideLoading()
//                }
//                hideLoading()
//            }
        }
    }

}