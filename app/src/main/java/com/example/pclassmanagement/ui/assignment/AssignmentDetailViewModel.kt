package com.example.pclassmanagement.ui.assignment

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.api.ServiceClient
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.StorageRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class AssignmentDetailViewModel @Inject constructor(
    private val contentRepository: ContentRepository,
    private val userRepository: UserRepository,
    private val storageRepository: StorageRepository,
) : ViewModel() {

    private var _agmResult = MutableLiveData<MarkResponse>()
    var agmResult = _agmResult as LiveData<MarkResponse>

    fun getScore(markRequest: MarkRequest) {
        ServiceClient.getService().getScore(markRequest).enqueue(
            object : retrofit2.Callback<MarkResponse> {
                override fun onResponse(
                    call: Call<MarkResponse>,
                    response: Response<MarkResponse>
                ) {
                    if (response.code() == 200) {
                        Log.d("res", response.toString())
                        _agmResult.postValue(response.body())
                    }
                }

                override fun onFailure(call: Call<MarkResponse>, t: Throwable) {
                    Log.d("error", t.toString())
                }

            }
        )
    }

    private val _submissionFileUrl =
        MutableSharedFlow<String>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val submissionFileUrl: SharedFlow<String>
        get() = _submissionFileUrl

    fun submitFile(selectedFile: Uri, classId: String, assignmentId: String) {
        viewModelScope.launch {
            storageRepository.uploadSubmission(selectedFile, classId, assignmentId).collectLatest {
                _submissionFileUrl.tryEmit(it.data.toString())
            }
        }
    }

    private val _submissionResponse =
        MutableSharedFlow<Boolean>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val submissionResponse: SharedFlow<Boolean>
        get() = _submissionResponse

    fun submitWork(classId: String, assignmentId: String, fileUrl: String?, fileName: String, dueDate: Long) {
        viewModelScope.launch {
            contentRepository.submitWork(classId, assignmentId, fileUrl = fileUrl, fileName, dueDate)
                .collectLatest {
                    _submissionResponse.tryEmit(true)
                }
        }
    }

    private val _getSubmissionByIdResponse =
        MutableSharedFlow<Submission?>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val getSubmissionByIdResponse: SharedFlow<Submission?>
        get() = _getSubmissionByIdResponse

    fun getSubmissionByUserId(classId: String, assignmentId: String) {
        viewModelScope.launch {
            contentRepository.getSubmissionByUserId(classId, assignmentId).collectLatest {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    _getSubmissionByIdResponse.tryEmit(it.data)
                }
            }
        }
    }

    private val _cancelSubmittedWorkResponse =
        MutableSharedFlow<Boolean>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val cancelSubmittedWorkResponse: SharedFlow<Boolean>
        get() = _cancelSubmittedWorkResponse

    fun cancelSubmittedWork(classId: String, assignmentId: String) {
        viewModelScope.launch {
            contentRepository.cancelSubmittedWork(classId, assignmentId).collectLatest {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    it.data?.let { it1 -> _cancelSubmittedWorkResponse.tryEmit(it1) }
                }
            }
        }
    }

    private val _getAllSubmissionResponse =
        MutableSharedFlow<List<Submission>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val getAllSubmissionResponse: SharedFlow<List<Submission>>
        get() = _getAllSubmissionResponse

    fun getAllSubmission(classId: String, assignmentId: String) {
        viewModelScope.launch {
            contentRepository.getAllSubmission(classId, assignmentId).collectLatest {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    it.data?.let { it1 ->
                        _getAllSubmissionResponse.tryEmit(it1)
                    }
                }
            }
        }
    }

    private val _userInfo =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val userInfo: SharedFlow<ResultWrapper<User>>
        get() = _userInfo

    fun getUserInfo(userId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(userId).collect {
                _userInfo.tryEmit(it)
            }
        }
    }

    private val _sendNoteResponse =
        MutableSharedFlow<Note>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val sendNoteResponse: SharedFlow<Note>
        get() = _sendNoteResponse

    fun sendNote(assignmentId: String, userId: String, note: String) {
        viewModelScope.launch {
            contentRepository.sendNote(assignmentId, userId, note).collect {
                it.data?.let { it1 -> _sendNoteResponse.tryEmit(it1) }
            }
        }
    }

    private val _sendMarkResponse =
        MutableSharedFlow<Mark>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val sendMarkResponse: SharedFlow<Mark>
        get() = _sendMarkResponse

    fun sendMark(assignmentId: String, userId: String, mark: Double) {
        viewModelScope.launch {
            contentRepository.sendMark(assignmentId, userId, mark).collect {
                it.data?.let { it1 -> _sendMarkResponse.tryEmit(it1) }
            }
        }
    }

    private val _getMarkResponse =
        MutableSharedFlow<Mark?>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val getMarkResponse: SharedFlow<Mark?>
        get() = _getMarkResponse

    fun getMarkByUserId(assignmentId: String) {
        viewModelScope.launch {
            contentRepository.getMarkByUserId(assignmentId).collect {
                if(it.status == ResultWrapper.Status.SUCCESS){
                    _getMarkResponse.tryEmit(it.data)
                }else{
                    _getMarkResponse.tryEmit(null)
                }
            }
        }

    }

    private val _getNoteResponse =
        MutableSharedFlow<Note?>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val getNoteResponse: SharedFlow<Note?>
        get() = _getNoteResponse

    fun getNoteByUserId(assignmentId: String) {
        viewModelScope.launch {
            contentRepository.getNoteByUserId(assignmentId).collect {
                if(it.status == ResultWrapper.Status.SUCCESS){
                    _getNoteResponse.tryEmit(it.data)
                }else{
                    _getNoteResponse.tryEmit(null)
                }
            }
        }

    }

}