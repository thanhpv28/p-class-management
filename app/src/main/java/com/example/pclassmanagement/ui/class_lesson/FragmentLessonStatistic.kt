package com.example.pclassmanagement.ui.class_lesson

import android.app.Dialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.FragmentLessonARGS
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.databinding.FragmentLessonStatisticBinding
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class FragmentLessonStatistic : DialogFragment() {

    private lateinit var binding: FragmentLessonStatisticBinding
    private val lessonViewModel: LessonViewModel by viewModels()
    private var lessonId = ""
    private var classId = ""
    private var teacherId = ""
    private lateinit var lesson: Lesson

    private lateinit var feedbackAdapter: FeedbackAdapter
    private var feedbackList = mutableListOf<LessonFeedBack>()

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val post = actionWrapper?.payload as CommentData
            when (actionWrapper.name) {

            }
        }
    })

    private lateinit var barChart: BarChart
    private lateinit var barData: BarData
    private lateinit var barDataSet: BarDataSet
    private lateinit var barEntriesList: ArrayList<BarEntry>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lessonId = arguments?.getString(FragmentLessonARGS.KEY.LESSON_ID).toString()
        classId = arguments?.getString(FragmentLessonARGS.KEY.CLASS_ID).toString()
        teacherId = arguments?.getString(FragmentLessonARGS.KEY.TEACHER_ID).toString()
        lesson = arguments?.getSerializable(FragmentLessonARGS.KEY.LESSON) as Lesson
        lessonViewModel.getAttendanceStatic(lessonId, classId)
        lessonViewModel.getLessonFeedback(lessonId)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentLessonStatisticBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val dialog: Dialog? = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.tvStartDate.text =
                "Thời gian bắt đầu: ${Utils.getDateTimeFormatted(lesson.startDate)}"
            binding.tvFinishedDate.text =
                "Thời gian kết thúc: ${Utils.getDateTimeFormatted(lesson.finishedDate)}"
        }

        initListener()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initListener() {
        lifecycleScope.launch {
            lessonViewModel.attendanceStatistic.collect {
                setUpAttendanceChart(it)
            }
        }

        lifecycleScope.launch {
            lessonViewModel.feedbackData.collect {
                setUpFeedbackChart(it)
                setUpFeedbackList(it)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setUpAttendanceChart(lessonAttendanceStatistic: LessonAttendanceStatistic) {
        val mChart = binding.chartAttendance
        mChart.isRotationEnabled = true
        mChart.description = Description().apply { text = "" }
        mChart.holeRadius = 35f
        mChart.setTransparentCircleAlpha(0)
        mChart.centerText = "Điểm danh"
        mChart.setCenterTextSize(10f)
        mChart.setDrawEntryLabels(true)
        mChart.setEntryLabelTextSize(0f)
        mChart.setEntryLabelColor(resources.getColor(R.color.white, context?.theme))

        val xEntrys = mutableListOf<String>()
        val yEntrys = mutableListOf<PieEntry>()
        val yData = floatArrayOf(
            lessonAttendanceStatistic.inTime.toFloat(),
            lessonAttendanceStatistic.late.toFloat(),
             if(lessonAttendanceStatistic.absent.toFloat() < 0) 15.toFloat() else lessonAttendanceStatistic.absent.toFloat(),
//            lessonAttendanceStatistic.absent.toFloat()
        )
        val xData = arrayOf("Đúng giờ", "Muộn", "Vắng mặt")

        for (i in yData.indices) {
            yEntrys.add(PieEntry(yData[i], xData[i]))
        }
        for (element in xData) {
            xEntrys.add(element)
        }


        val pieDataSet = PieDataSet(yEntrys, "Điểm danh")
        pieDataSet.sliceSpace = 2f
        pieDataSet.valueTextSize = 12f

        val colors: ArrayList<Int> = ArrayList()
        colors.add(resources.getColor(R.color.color_success, context?.theme))
        colors.add(resources.getColor(R.color.orange, context?.theme))
        colors.add(resources.getColor(R.color.color_error, context?.theme))

        pieDataSet.colors = colors

        val legend: Legend = binding.chartAttendance.legend
        legend.form = Legend.LegendForm.CIRCLE
//        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART)

        val pieData = PieData(pieDataSet)
        binding.chartAttendance.data = pieData
        binding.chartAttendance.invalidate()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setUpFeedbackChart(feedbackData: FeedbackData) {
        barChart = binding.chartFeedback

        val xAxisValues = listOf("0", "1", "2", "3", "4", "5")
        barChart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return xAxisValues.get(index = value.toInt())
            }
        }
        binding.tvFeedbackStar.text = feedbackData.ratingAverage.toString()
        binding.ratingFeedback.rating = feedbackData.ratingAverage

        barEntriesList = ArrayList()
        barEntriesList.add(BarEntry(1f, feedbackData.rating1))
        barEntriesList.add(BarEntry(2f, feedbackData.rating2))
        barEntriesList.add(BarEntry(3f, feedbackData.rating3))
        barEntriesList.add(BarEntry(4f, feedbackData.rating4))
        barEntriesList.add(BarEntry(5f, feedbackData.rating5))

        barDataSet = BarDataSet(barEntriesList, "Đánh giá của học viên")
        barData = BarData(barDataSet)
        barData.barWidth = 0.6f
        barChart.data = barData

        barDataSet.valueTextColor = Color.BLACK
        barDataSet.color = resources.getColor(R.color.color_success, context?.theme)
        barDataSet.valueTextSize = 10f
        barChart.description.isEnabled = false
//        barChart.setFitBars(true)
        barChart.isDragEnabled = false
        barChart.show()

    }

    private fun setUpFeedbackList(feedbackData: FeedbackData) {
        // init rv
        feedbackList.clear()
        feedbackList.addAll(feedbackData.feedbackList.reversed())
        feedbackAdapter = FeedbackAdapter(actionDispatcher, feedbackList)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvFeedback.layoutManager = layoutManager
        binding.rvFeedback.adapter = feedbackAdapter
    }

}