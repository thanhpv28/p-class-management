package com.example.pclassmanagement.ui.class_lesson

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.ContributionPoint
import com.example.pclassmanagement.data.ContributionType
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.log
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LessonViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val contentRepository: ContentRepository
) : ViewModel() {

    private val _classDetail =
        MutableSharedFlow<ResultWrapper<ClassData>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val classDetail: SharedFlow<ResultWrapper<ClassData>>
        get() = _classDetail

    private val _ownerInfo =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val ownerInfo: SharedFlow<ResultWrapper<User>>
        get() = _ownerInfo

    private val _attendanceStatistic =
        MutableSharedFlow<LessonAttendanceStatistic>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val attendanceStatistic: SharedFlow<LessonAttendanceStatistic>
        get() = _attendanceStatistic

    private val _currentUser =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val currentUser: SharedFlow<ResultWrapper<User>>
        get() = _currentUser

    private val _feedbackStatus =
        MutableSharedFlow<Boolean>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val feedbackStatus: SharedFlow<Boolean>
        get() = _feedbackStatus

    private val _feedbackData =
        MutableSharedFlow<FeedbackData>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val feedbackData: SharedFlow<FeedbackData>
        get() = _feedbackData

    fun getAttendanceStatic(lessonId: String, classId: String) {

        viewModelScope.launch {

            contentRepository.getMemberCountByClassId(classId).collect { totalMemberResponse ->
                if (totalMemberResponse.status == ResultWrapper.Status.SUCCESS) {
                    val totalMember: Int = totalMemberResponse.data!!
                    log(totalMember.toString() , "member: ")

                    contentRepository.getAttendanceByLessonId(lessonId)
                        .collect { attendanceResponse ->
                            if (attendanceResponse.status == ResultWrapper.Status.SUCCESS) {
                                val inTime: Int
                                var late = 0
                                val absent: Int

                                val attended: Int = (attendanceResponse.data ?: listOf()).size
                                attendanceResponse.data?.forEach {
                                    if (it.late) late++
                                }
                                inTime = attended - late
                                absent = totalMember - attended

                                _attendanceStatistic.tryEmit(
                                    LessonAttendanceStatistic(
                                        totalMember = totalMember,
                                        attended = attended,
                                        inTime = inTime,
                                        late = late,
                                        absent = absent,
                                    )
                                )
                                log(LessonAttendanceStatistic(
                                    totalMember = totalMember,
                                    attended = attended,
                                    inTime = inTime,
                                    late = late,
                                    absent = absent,
                                ).toString(), "statistic")

                            }
                        }
                }
            }

        }
    }

    fun sendFeedBack(lessonFeedBack: LessonFeedBack, classId: String) {
        viewModelScope.launch {
            contentRepository.sendFeedBack(lessonFeedBack).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    _feedbackStatus.tryEmit(true)
                    contentRepository.addContribution(
                        classId = classId,
                        type = ContributionType.FEEDBACK,
                        point = ContributionPoint.FEEDBACK,
                    )
                }
            }
        }
    }

    fun getLessonFeedback(lessonId: String) {
        viewModelScope.launch {
            contentRepository.getLessonFeedBack(lessonId).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    var rating1 = 0
                    var rating2 = 0
                    var rating3 = 0
                    var rating4 = 0
                    var rating5 = 0
                    var ratingAverage = 0f

                    it.data?.forEach { lessonFeedbackItem ->
                        ratingAverage += lessonFeedbackItem.rating
                        if (lessonFeedbackItem.rating >= 0 && lessonFeedbackItem.rating < 2) {
                            rating1++
                        }
                        if (lessonFeedbackItem.rating >= 2 && lessonFeedbackItem.rating < 3) {
                            rating2++
                        }
                        if (lessonFeedbackItem.rating >= 3 && lessonFeedbackItem.rating < 4) {
                            rating3++
                        }
                        if (lessonFeedbackItem.rating >= 4 && lessonFeedbackItem.rating < 5) {
                            rating4++
                        }
                        if (lessonFeedbackItem.rating == 5f) {
                            rating5++
                        }
                    }

                    if (it.data != null && it.data.isNotEmpty())
                        ratingAverage /= it.data.size

                    _feedbackData.tryEmit(
                        FeedbackData(
                            rating1.toFloat(),
                            rating2.toFloat(),
                            rating3.toFloat(),
                            rating4.toFloat(),
                            rating5.toFloat(),
                            ratingAverage,
                            it.data ?: listOf()
                        )
                    )


                }
            }
        }
    }

    fun getUserInfo(userId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(userId).collect {
                _currentUser.tryEmit(it)
            }
        }
    }
}