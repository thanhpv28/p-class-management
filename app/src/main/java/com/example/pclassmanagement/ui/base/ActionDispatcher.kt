package com.example.pclassmanagement.ui.base

import com.example.pclassmanagement.data.model.ActionWrapper

interface ActionExecutor {
    fun dispatch(actionWrapper: ActionWrapper?)
}

class ActionDispatcher(private val actionExecutor: ActionExecutor){
    fun dispatch(actionWrapper: ActionWrapper? = null){
        actionExecutor.dispatch(actionWrapper)
    }
}