package com.example.pclassmanagement.ui.landing_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.pclassmanagement.R
import com.example.pclassmanagement.ui.base.BaseLoadingDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create_class.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FragmentCreateClass: DialogFragment() {

    private val landingPageViewModel: LandingPageViewModel by viewModels()

    private lateinit var fireStore: FirebaseFirestore
    private lateinit var mRef: DatabaseReference
    private lateinit var auth: FirebaseAuth

    private val loadingDialog by lazy {
        BaseLoadingDialog()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fireStore = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_create_class, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_cancel.setOnClickListener {
            dismiss()
        }

        bt_create.setOnClickListener {
            createClass()
        }

        lifecycleScope.launch {
            landingPageViewModel.createClassSuccess.collect {
                loadingDialog.dismiss()
                if(it.data == true){
                    Toast.makeText(context, "Tạo lớp học thành công!", Toast.LENGTH_SHORT).show()
                    dismiss()
                }
            }
        }
    }

    private fun createClass() {
        val className = et_class_name.text.toString()
        if(className.isEmpty()) {
            Toast.makeText(context, "Tên lớp học không được để trống!", Toast.LENGTH_SHORT).show()
            return
        }
        loadingDialog.show(childFragmentManager, null)
        val classDes = et_class_des.text.toString()
        landingPageViewModel.createClass(className, classDes)
    }

}