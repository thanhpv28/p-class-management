package com.example.pclassmanagement.ui.assignment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pclassmanagement.data.FragmentAssignmentARGS
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.log
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.databinding.FragmentAssignmentDetailBinding
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class FragmentAssignmentDetail : BaseFragment() {

    private val assignmentDetailViewModel: AssignmentDetailViewModel by viewModels()

    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val userRef = FirebaseDatabase.getInstance().getReference(Table.USER)
    var classId = ""
    var ownerId = ""
    private var finalAns = ""
    private var selectedFile: Uri? = null
    private var fileName = ""

    private lateinit var assignment: Assignment
    private lateinit var binding: FragmentAssignmentDetailBinding

    private lateinit var multiChoiceAdapter: AssignmentMultiChoiceAdapter
    private var ans = hashMapOf<Int, String>()

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val position = actionWrapper?.payload as Int
            when (actionWrapper.name) {
                "a" -> {
                    ans[position] = "a"
                }
                "b" -> {
                    ans[position] = "b"
                }
                "c" -> {
                    ans[position] = "c"
                }
                "d" -> {
                    ans[position] = "d"
                }
            }

        }
    })

    override fun initData() {
        super.initData()
        classId = arguments?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        ownerId = arguments?.getString(FragmentLandingPageARGS.KEY.OWNER_ID).toString()
        assignment = arguments?.getSerializable(FragmentAssignmentARGS.KEY.ASSIGNMENT) as Assignment
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAssignmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initView() {
        super.initView()
        if (firebaseUser?.uid.toString() == ownerId) {
            binding.btViewAllSubmissions.show()
            binding.btSubmit.gone()
            binding.btChooseFile.gone()
        }else{
            showLoading()
            assignmentDetailViewModel.getMarkByUserId( assignment.id)
            assignmentDetailViewModel.getNoteByUserId( assignment.id)
        }

        userRef.child(ownerId).get().addOnCompleteListener {
            if (it.isSuccessful) {
                val user = it.result.getValue(User::class.java)
                binding.tvAgmUsername.text = user?.fullname
                Glide.with(requireContext())
                    .load(user?.imageUrl)
                    .into(binding.ivAssignmentOwner)
            }
        }

//        if (assignment.type == 1) {
//            multiChoiceAdapter = AssignmentMultiChoiceAdapter(actionDispatcher, MultiChoice().data)
//            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//            binding.rvAgmMultiChoice.layoutManager = layoutManager
//            binding.rvAgmMultiChoice.adapter = multiChoiceAdapter
//        } else
//            binding.btChooseFile.show()

        binding.tvAgmTitle.text = assignment.title
        binding.tvAgmDetail.text = assignment.content.replace("\\n", "\n")
        binding.tvAgmDueDate.text = "Đến hạn " + Utils.getDateFromMillisecond(assignment.dueDate)
        binding.tvAgmCreatedDate.text =
            "Lúc " + Utils.getDateFromMillisecond(assignment.createdDate)
    }

    override fun initListener() {
        super.initListener()

        binding.btSubmit.setOnClickListener {
            showLoading()
            if (selectedFile != null) {
                submitFile()
            } else {
                submitWork("")
            }
        }

        binding.btViewAllSubmissions.setOnClickListener {
            startActivity(Intent(context, ActivityViewAllSubmissions::class.java).apply {
                putExtra("assignmentId", assignment.id)
                putExtra("classId", classId)
            })
        }

        assignmentDetailViewModel.agmResult.observe(viewLifecycleOwner, Observer {
            hideLoading()
            if (it.statusCode == 200) {

                GlobalScope.launch(Dispatchers.Main) {
                    delay(2000)
                    DialogMarkSuccess(score = it.score).show(childFragmentManager, null)
                }
            }
        })

        binding.btChooseFile.setOnClickListener {
            val intent = Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(Intent.createChooser(intent, "Chọn file"), 111)
        }

        binding.btClearSelectedFile.setOnClickListener {
            selectedFile = null
            fileName = ""
            binding.groupFile.gone()
        }

        binding.btCancelSubmit.setOnClickListener {
            showLoading()
            assignmentDetailViewModel.cancelSubmittedWork(classId, assignment.id)
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.submissionFileUrl.collectLatest {
                submitWork(fileUrl = it)
                selectedFile = null
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.getMarkResponse.collectLatest {
                if(it == null){
                    assignmentDetailViewModel.getSubmissionByUserId(classId, assignment.id)
                }else{
                    hideLoading()
                    binding.groupMark.show()
                    binding.tvMark.text = "Điểm của bạn: " + it.mark
                }
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.getNoteResponse.collectLatest {
                if(it == null){
                    return@collectLatest
                }else{
                    binding.etSubmissionNote.setText(it.note)
                }
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.getSubmissionByIdResponse.collectLatest {
                hideLoading()
                // user has already submitted work
                if(it != null){
                    binding.btSubmit.gone()
                    binding.btChooseFile.gone()
                    binding.btCancelSubmit.show()
                    binding.groupSubmittedFile.show()
                    binding.tvFileNameSubmitted.text = it.fileName
                }else{
                    binding.btSubmit.show()
                    binding.btChooseFile.show()
                }

            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.submissionResponse.collectLatest {
                if(it){
                    hideLoading()
                    binding.tvFileNameSubmitted.text = fileName
                    selectedFile = null
                    fileName = ""
                    binding.btSubmit.gone()
                    binding.btChooseFile.gone()
                    binding.groupFile.gone()
                    binding.groupSubmittedFile.show()
                    binding.btCancelSubmit.show()
                    showToast("Nộp bài thành công")
                }
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.cancelSubmittedWorkResponse.collectLatest {
                if(it){
                    hideLoading()
                    selectedFile = null
                    fileName = ""
                    binding.groupSubmittedFile.gone()
                    binding.btCancelSubmit.gone()
                    binding.btSubmit.show()
                    binding.btChooseFile.show()
                    showToast("Huỷ nộp bài thành công!")
                }else{
                    showToast("Huỷ nộp bài không thành công")

                }
            }
        }
    }

    @Deprecated(
        "Deprecated in Java", ReplaceWith(
            "super.onActivityResult(requestCode, resultCode, data)",
            "com.example.dclassmanagement.ui.base.BaseFragment"
        )
    )
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == RESULT_OK) {
            selectedFile = data?.data!! //The uri with the location of the file
            fileName = Utils.getFileName(requireContext(), selectedFile!!).toString()
            binding.groupFile.show()
            binding.tvFileName.text = fileName
        }
    }

    private fun submitFile() {
        if (selectedFile != null) {
            assignmentDetailViewModel.submitFile(selectedFile!!, classId, assignment.id)
        } else {

        }
    }

    private fun submitWork(fileUrl: String?) {
        assignmentDetailViewModel.submitWork(classId, assignment.id, fileUrl = fileUrl, fileName, dueDate = assignment.dueDate)
    }

}