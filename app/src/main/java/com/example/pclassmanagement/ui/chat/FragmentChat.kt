package com.example.pclassmanagement.ui.chat

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.FragmentChatARGS
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.local.LocalDataAccess
import com.example.pclassmanagement.data.log
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Chat
import com.example.pclassmanagement.databinding.FragmentChatBinding
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ActivityChat : BaseActivity() {

    private lateinit var binding: FragmentChatBinding
    private val chatRef = FirebaseDatabase.getInstance().getReference(Table.CHAT)

    @Inject
    lateinit var localDataAccess: LocalDataAccess

    private var chatList = mutableListOf<Chat>()

    private lateinit var chatAdapter: ChatAdapter

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val item = actionWrapper?.payload as Chat
//            requestNavigate(
//                R.id.fragmentMessage, bundleOf(
//                    FragmentChatARGS.KEY.USER_ID to item
//                )
//            )
            var uidTarget = item.uid1
            if(localDataAccess.getUserId() == item.uid1) {
                uidTarget = item.uid2
            }
            startActivity(Intent(baseContext, ActivityMessage::class.java).apply {
                putExtra(FragmentChatARGS.KEY.USER_ID, uidTarget)
                putExtra(FragmentChatARGS.KEY.CHAT_ID, item.id)
            })

        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_chat)
    }

    override fun initData() {
        super.initData()
        intent.getBooleanExtra(FragmentChatARGS.KEY.IS_SHOW_MESSAGE_DETAIL, false).run {
            if (this) {
                startActivity(Intent(baseContext, ActivityMessage::class.java).apply {
                    putExtra(
                        FragmentChatARGS.KEY.USER_ID,
                        intent.getStringExtra(FragmentChatARGS.KEY.USER_ID)
                    )
                })
            }
        }
    }

    override fun initView() {
        super.initView()
        chatAdapter = ChatAdapter(actionDispatcher, chatList)
        val layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        binding.rvChat.layoutManager = layoutManager
        binding.rvChat.adapter = chatAdapter

        chatRef.addValueEventListener(object : ValueEventListener {
            @SuppressLint("NotifyDataSetChanged")
            override fun onDataChange(snapshot: DataSnapshot) {
                log(snapshot.toString())
                chatList.clear()
                snapshot.children.forEach {
                    val chat = it.getValue(Chat::class.java)
                    if (chat != null && (chat.uid1 == localDataAccess.getUserId() || chat.uid2 == localDataAccess.getUserId())) {
                        chatList.add(chat)
                    }
                }
                chatList.sortByDescending { it.lastSentDate }
                log(chatList.toString())
                chatAdapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })
    }

    override fun initListener() {
        super.initListener()
        binding.btnBack.setOnClickListener {
            finish()
        }
    }


}