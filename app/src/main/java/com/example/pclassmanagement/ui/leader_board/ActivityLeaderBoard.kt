package com.example.pclassmanagement.ui.leader_board

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.data.model.UserContributionData
import com.example.pclassmanagement.databinding.ActivityLeaderBoardBinding
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseActivity
import com.example.pclassmanagement.ui.class_detail.ClassViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ActivityLeaderBoard : BaseActivity() {

    private val classViewModel: ClassViewModel by viewModels()
    lateinit var classId: String
    private val userRef = FirebaseDatabase.getInstance().getReference(Table.USER)

    private lateinit var binding: ActivityLeaderBoardBinding

    //recycler view
    private lateinit var leaderBoardAdapter: LeaderBoardAdapter
    private var userContributionDataList = mutableListOf<UserContributionData>()

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {

        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeaderBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun initData() {
        super.initData()
        classId = intent.getStringExtra("classId").toString()
    }

    override fun initView() {
        super.initView()
        classViewModel.getClassContributionLeaderboard(classId)
        showLoading()
        // init rv
        leaderBoardAdapter = LeaderBoardAdapter(actionDispatcher, userContributionDataList)
        val layoutManager =
            LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        binding.rvContributionLeaderBoard.layoutManager = layoutManager
        binding.rvContributionLeaderBoard.adapter = leaderBoardAdapter


    }

    @SuppressLint("NotifyDataSetChanged")
    override fun initListener() {
        super.initListener()

        lifecycleScope.launch {
            classViewModel.userContributionList.collectLatest {
                hideLoading()
                userContributionDataList.clear()
                userContributionDataList.addAll(it.sortedBy { item -> item.totalPoint }.reversed())
                leaderBoardAdapter.notifyDataSetChanged()
                setUpTopOfLeaderBoard(userContributionDataList)
            }
        }
    }

    private fun setUpTopOfLeaderBoard(userContributionDataList: List<UserContributionData>) {
        if (userContributionDataList.isNotEmpty()) {
            binding.tvPoint1.text = userContributionDataList[0].totalPoint.toString()

            val userContribution0 =
                userContributionDataList[0].userContribution
            userRef.child(userContribution0.userId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)
                        binding.tvFullName1.text = user?.fullname
                        binding.tvUsername1.text =
                            if (user?.studentCode?.isEmpty() == true) "Chưa cập nhật" else user?.studentCode
                        Glide.with(baseContext)
                            .load(user?.imageUrl)
                            .placeholder(R.drawable.ic_person)
                            .into(binding.ivRank1)
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }
                })
        }

        if (userContributionDataList.size > 1) {
            binding.tvPoint2.text = userContributionDataList[1].totalPoint.toString()

            val userContribution1 =
                userContributionDataList[1].userContribution
            userRef.child(userContribution1.userId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)
                        binding.tvFullName2.text = user?.fullname
                        binding.tvUsername2.text =
                            if (user?.studentCode?.isEmpty() == true) "Chưa cập nhật" else user?.studentCode
                        Glide.with(baseContext)
                            .load(user?.imageUrl)
                            .placeholder(R.drawable.ic_person)
                            .into(binding.ivRank2)
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }
                })
        }

        if (userContributionDataList.size > 2) {
            binding.tvPoint3.text = userContributionDataList[2].totalPoint.toString()

            val userContribution2 =
                userContributionDataList[2].userContribution
            userRef.child(userContribution2.userId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = snapshot.getValue(User::class.java)
                        binding.tvFullName3.text = user?.fullname
                        binding.tvUsername3.text =
                            if (user?.studentCode?.isEmpty() == true) "Chưa cập nhật" else user?.studentCode
                        Glide.with(baseContext)
                            .load(user?.imageUrl)
                            .placeholder(R.drawable.ic_person)
                            .into(binding.ivRank3)
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }
                })
        }


    }
}