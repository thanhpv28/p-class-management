package com.example.pclassmanagement.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.example.pclassmanagement.R
import com.example.pclassmanagement.MainActivity
import com.example.pclassmanagement.ui.auth.LoginActivity
import com.example.pclassmanagement.ui.base.BaseActivity
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashScreenActivity : BaseActivity() {
    private lateinit var mAuth: FirebaseAuth

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_splash_screen)
        mAuth = FirebaseAuth.getInstance()

        GlobalScope.launch(Dispatchers.Main) {
            delay(1000)
            if(mAuth.currentUser != null)
                startActivity(Intent(this@SplashScreenActivity, MainActivity::class.java))
            else
                startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
            finish()
        }


        super.onCreate(savedInstanceState)
    }
}