package com.example.pclassmanagement.ui.comment

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.data.FragmentHomeARGS
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.CommentData
import com.example.pclassmanagement.data.model.Post
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.databinding.FragmentCommentBinding
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.class_detail.ClassViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FragmentComment : BottomSheetDialogFragment() {

    private val classViewModel: ClassViewModel by viewModels()
    private val likeRef = FirebaseDatabase.getInstance().getReference(Table.LIKE)
    private val commentRef = FirebaseDatabase.getInstance().getReference(Table.COMMENT)
    private val firebaseUser = FirebaseAuth.getInstance().currentUser

    private lateinit var post: Post
    private lateinit var binding: FragmentCommentBinding

    private lateinit var commentAdapter: CommentAdapter
    private var commentList = mutableListOf<CommentData>()

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val post = actionWrapper?.payload as CommentData
            when (actionWrapper.name) {

            }
        }
    })


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCommentBinding.inflate(inflater, container, false)
        initData()
        initView()
        initListener()
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout =
                bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            parentLayout?.let { it ->
                val behaviour = BottomSheetBehavior.from(it)
                setupFullHeight(it)
                behaviour.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    private fun setupFullHeight(bottomSheet: View) {
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        bottomSheet.layoutParams = layoutParams
    }

     fun initData() {
        post = arguments?.getSerializable(FragmentHomeARGS.KEY.POST) as Post
    }

     fun initView() {
        // init rv
        commentAdapter = CommentAdapter(actionDispatcher, commentList)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvComment.layoutManager = layoutManager
        binding.rvComment.adapter = commentAdapter

        // init like count
        likeRef.child(post.id).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.childrenCount == 0L)
                    binding.tvLikeCount.text = "Hãy là người đầu tiên thích bài viết này"
                else
                    binding.tvLikeCount.text = "${snapshot.childrenCount} người thích bài viết này"
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

        // init comment count
        val commentQuery = commentRef.orderByChild("postId").equalTo(post.id)
        commentQuery.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.childrenCount == 0L)
                    binding.tvNoComment.show()
                else {
                    binding.tvNoComment.gone()
                    val oldSize = commentList.size
                    commentList.clear()
                    snapshot.children.forEach { snap ->
                        snap.getValue(CommentData::class.java)?.let { commentList.add(it) }
                    }
                    commentAdapter.notifyItemRangeInserted(oldSize, commentList.size)
                    binding.rvComment.smoothScrollToPosition(commentAdapter.itemCount - 1)
                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

     fun initListener() {
        binding.btSendComment.setOnClickListener {
            val content = binding.etComment.text.toString()
            if (content.isNotEmpty()) {
                classViewModel.addComment( postId = post.id, classId = post.classId,  content = content.trim())
                binding.etComment.text = Editable.Factory.getInstance().newEditable("")
            }
        }
    }

}