package com.example.pclassmanagement.ui.landing_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.databinding.FragmentJoinClassBinding
import com.example.pclassmanagement.ui.base.BaseLoadingDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_create_class.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FragmentJoinClass : DialogFragment() {

    private val landingPageViewModel: LandingPageViewModel by viewModels()
    private lateinit var fireStore: FirebaseFirestore
    private lateinit var mRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private lateinit var binding: FragmentJoinClassBinding

    private val loadingDialog by lazy {
        BaseLoadingDialog()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fireStore = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentJoinClassBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_cancel.setOnClickListener {
            dismiss()
        }

        binding.btJoin.setOnClickListener {
            joinClass()
        }

        lifecycleScope.launch {
            landingPageViewModel.joinClassSuccess.collect {

                if(it.status == ResultWrapper.Status.SUCCESS){
                    Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }
                dismiss()
            }
        }
    }

    private fun joinClass() {
        if(binding.etClassId.text.isNullOrEmpty()) {
            Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show()
            return
        }
        landingPageViewModel.joinClass(
            userId = auth.currentUser?.uid.toString(),
            classCode = binding.etClassId.text.toString()
        )
    }

}