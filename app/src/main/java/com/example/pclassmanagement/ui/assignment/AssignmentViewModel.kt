package com.example.pclassmanagement.ui.assignment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pclassmanagement.data.api.ServiceClient
import com.example.pclassmanagement.data.local.SharedPreferencesHelper
import com.example.pclassmanagement.data.model.MarkRequest
import com.example.pclassmanagement.data.model.MarkResponse
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class AssignmentViewModel  @Inject constructor(
    private val contentRepository: ContentRepository,
): ViewModel() {

    private var _agmResult = MutableLiveData<MarkResponse>()
    var agmResult = _agmResult as LiveData<MarkResponse>

    fun getScore(markRequest: MarkRequest){
        ServiceClient.getService().getScore(markRequest).enqueue(
            object : retrofit2.Callback<MarkResponse>{
                override fun onResponse(
                    call: Call<MarkResponse>,
                    response: Response<MarkResponse>
                ) {
                    if(response.code() == 200){
                        Log.d("res", response.toString())
                        _agmResult.postValue(response.body())
                    }
                }

                override fun onFailure(call: Call<MarkResponse>, t: Throwable) {
                    Log.d("error", t.toString())
                }

            }
        )
    }

}