package com.example.pclassmanagement.ui.assignment

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.model.Note
import com.example.pclassmanagement.data.model.Submission
import com.example.pclassmanagement.databinding.ActivitySubmissionDetailBinding
import com.example.pclassmanagement.databinding.ActivityViewSubmitionBinding
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.item_submission.view.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ActivitySubmissionDetail : BaseActivity() {

    private lateinit var binding: ActivitySubmissionDetailBinding
    private lateinit var submission: Submission
    private val assignmentDetailViewModel: AssignmentDetailViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_submission_detail)
    }

    override fun initData() {
        super.initData()
        submission = intent.getSerializableExtra("submission") as Submission
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initView() {
        super.initView()
        assignmentDetailViewModel.getUserInfo(submission.userId)

        binding.tvItemFileName.text = submission.fileName
        binding.tvItemSubmittedDate.text = Utils.getDateFromMillisecond(submission.createdDate)
        if(submission.late){
            binding.tvItemStatus.text = "Nộp muộn"
            binding.tvItemStatus.setTextColor(resources.getColor(R.color.color_error, theme))
        }
    }

    override fun initListener() {
        super.initListener()

        binding.btSendResult.setOnClickListener {
            val mark = binding.etSubmissionMark.text.toString()
            if(mark.isEmpty()) {
                showToast("Bạn cần nhập điểm cho sinh viên")
                return@setOnClickListener
            }
            if(mark.toDouble() < 0 || mark.toDouble() > 10){
                showToast("Điểm số cần >= 0 và <= 10")
                return@setOnClickListener
            }
            showLoading()
            val note = binding.etSubmissionNote.text.toString()
            if(note.isNotEmpty()){
                assignmentDetailViewModel.sendNote(submission.assignmentId, submission.userId, note)
            }else{
                sendResult(mark.toDouble())
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.userInfo.collectLatest {
                val user = it.data
                if (user != null) {
                    if (user.imageUrl.isNotEmpty()) {
                        binding.tvItemFullName.text = user.fullname + " -"
                        binding.tvItemMsv.text = user.studentCode
                        Glide.with(baseContext)
                            .load(user.imageUrl)
                            .into(binding.ivItemAvatar)
                    }
                }

            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.sendNoteResponse.collectLatest {
                val mark = binding.etSubmissionMark.text.toString().toDouble()
                sendResult(mark = mark,note = it)
            }
        }

        lifecycleScope.launch {
            assignmentDetailViewModel.sendMarkResponse.collectLatest {
                hideLoading()
                showToast("Chấm điểm thành công!")
            }
        }
    }

    private fun sendResult(mark: Double,note: Note? = null) {
        lifecycleScope.launch {
            assignmentDetailViewModel.sendMark(submission.assignmentId, submission.userId,mark )
        }
    }
}