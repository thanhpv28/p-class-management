package com.example.pclassmanagement.ui.image_viewer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R

class ActivityImageViewer : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)

        val src = intent.getStringExtra("src")

        Glide.with(baseContext)
            .load(src)
            .placeholder(R.drawable.placeholder)
            .into(findViewById(R.id.iv_view))
    }
}