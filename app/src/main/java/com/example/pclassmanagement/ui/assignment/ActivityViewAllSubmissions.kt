package com.example.pclassmanagement.ui.assignment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.FragmentAssignmentARGS
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Assignment
import com.example.pclassmanagement.data.model.Submission
import com.example.pclassmanagement.databinding.ActivityLoginBinding
import com.example.pclassmanagement.databinding.ActivityViewSubmitionBinding
import com.example.pclassmanagement.navigation.Navigator.requestNavigate
import com.example.pclassmanagement.ui.auth.AuthViewModel
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ActivityViewAllSubmissions : BaseActivity() {

    private lateinit var binding: ActivityViewSubmitionBinding
    private val assignmentDetailViewModel: AssignmentDetailViewModel by viewModels()

    private var assignmentId = ""
    private var classId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_submition)
    }

    private var submissionList = mutableListOf<Submission>()

    private lateinit var submissionAdapter: SubmissionAdapter

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val submission = actionWrapper?.payload as Submission

            startActivity(Intent(baseContext, ActivitySubmissionDetail::class.java).apply {
                putExtra("submission", submission)
            })
        }
    })

    override fun initData() {
        super.initData()
        assignmentId = intent.getStringExtra("assignmentId").toString()
        classId = intent.getStringExtra("classId").toString()
    }

    override fun initView() {
        super.initView()
        submissionAdapter = SubmissionAdapter(actionDispatcher, submissionList, baseContext)
        val layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.VERTICAL, false)
        binding.rvAssignment.layoutManager = layoutManager
        binding.rvAssignment.adapter = submissionAdapter

        assignmentDetailViewModel.getAllSubmission(classId, assignmentId)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun initListener() {
        super.initListener()
        lifecycleScope.launch{
            assignmentDetailViewModel.getAllSubmissionResponse.collectLatest {
                submissionList.clear()
                submissionList.addAll(it)
                submissionAdapter.notifyDataSetChanged()
            }
        }
    }


}