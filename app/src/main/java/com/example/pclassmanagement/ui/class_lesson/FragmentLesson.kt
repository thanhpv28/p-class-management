package com.example.pclassmanagement.ui.class_lesson

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.*
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Lesson
import com.example.pclassmanagement.databinding.FragmentLessonBinding
import com.example.pclassmanagement.navigation.Navigator.requestNavigate
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class FragmentLesson : BaseFragment() {

    private lateinit var binding: FragmentLessonBinding
    private val firebaseUser = FirebaseAuth.getInstance().currentUser
    private val lessonRef = FirebaseDatabase.getInstance().getReference(Table.LESSON)
    private val assignmentUserRef =
        FirebaseDatabase.getInstance().getReference(Table.ASSIGNMENT_USER)
    private var lessonList = mutableListOf<Lesson>()
    var classId = ""
    var ownerId = ""

    private lateinit var lessonAdapter: LessonAdapter
    private val lessonViewModel: LessonViewModel by viewModels()

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            when (actionWrapper?.name) {
                Action.Item.SHOW_TOAST -> {
                    val message = actionWrapper.payload as String
                    showToast(message)
                }
                Action.Item.LESSON_VIEW_STATISTIC -> {
                    val lesson = actionWrapper.payload as Lesson
                    val fragmentLessonStatistic = FragmentLessonStatistic()
                    fragmentLessonStatistic.arguments = bundleOf(
                        FragmentLessonARGS.KEY.LESSON_ID to lesson.id,
                        FragmentLessonARGS.KEY.CLASS_ID to classId,
                        FragmentLessonARGS.KEY.TEACHER_ID to ownerId,
                        FragmentLessonARGS.KEY.LESSON to lesson,
                    )
                    fragmentLessonStatistic.show(
                        childFragmentManager, null
                    )
                }

                Action.Item.LESSON_FEEDBACK -> {
                    val lesson = actionWrapper.payload as Lesson
                    val fragmentLessonStatistic = FragmentLessonFeedBack()
                    fragmentLessonStatistic.arguments = bundleOf(
                        FragmentLessonARGS.KEY.LESSON_ID to lesson.id,
                        FragmentLessonARGS.KEY.CLASS_ID to classId,
                        FragmentLessonARGS.KEY.TEACHER_ID to ownerId,
                        FragmentLessonARGS.KEY.LESSON to lesson,
                    )
                    fragmentLessonStatistic.show(
                        childFragmentManager, null
                    )
                }
                Action.Item.ATTENDANCE_IN_TIME -> {
                    sharedViewModel.addContribution(
                        classId = classId,
                        type = ContributionType.IN_TIME_ATTENDANCE,
                        point = ContributionPoint.IN_TIME_ATTENDANCE,
                    )
                }
                Action.Item.ATTENDANCE_LATE -> {
                    sharedViewModel.addContribution(
                        classId = classId,
                        type = ContributionType.LATE_ATTENDANCE,
                        point = ContributionPoint.LATE_ATTENDANCE,
                    )
                }

            }
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLessonBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initData() {
        classId = arguments?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        ownerId = arguments?.getString(FragmentLandingPageARGS.KEY.OWNER_ID).toString()
        super.initData()
    }

    override fun initView() {
        super.initView()
        lessonAdapter = LessonAdapter(
            actionDispatcher,
            lessonList,
            ownerId,
            firebaseUser!!.uid,
            requireContext()
        )
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvLesson.layoutManager = layoutManager
        binding.rvLesson.adapter = lessonAdapter

        lessonRef.addValueEventListener(object : ValueEventListener {
            @SuppressLint("NotifyDataSetChanged")
            override fun onDataChange(snapshot: DataSnapshot) {
                lessonList.clear()
                snapshot.children.forEach {
                    val lesson = it.getValue(Lesson::class.java)
                    if (lesson != null && lesson.classId == classId) {
                        lessonList.add(lesson)
                    }
                }
                lessonAdapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })
    }

    override fun initListener() {
        super.initListener()
        binding.btMember.setOnClickListener {
            requestNavigate(
                R.id.fragmentMember2, bundleOf(
                    FragmentLandingPageARGS.KEY.CLASS_ID to classId,
                    FragmentLandingPageARGS.KEY.OWNER_ID to ownerId
                )
            )
        }
//        if (firebaseUser?.uid == ownerId) {
//            binding.btCreateAssignment.show()
//            binding.btCreateAssignment.setOnClickListener {
//                FragmentCreateAssignment(classId).show(childFragmentManager, null)
//            }
//        }

    }
}