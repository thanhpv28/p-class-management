package com.example.pclassmanagement.ui.landing_page

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.data.FragmentProfileARGS
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.ClassData
import com.example.pclassmanagement.databinding.FragmentLandingPageBinding
import com.example.pclassmanagement.navigation.Navigator.requestNavigate
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.ui.auth.LoginActivity
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.example.pclassmanagement.ui.base.ActionExecutor
import com.example.pclassmanagement.ui.base.BaseFragment
import com.example.pclassmanagement.ui.chat.ActivityChat
import com.example.pclassmanagement.ui.class_detail.ActivityClassDetailHost
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_landing_page.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class FragmentLandingPage : BaseFragment() {

    private val landingPageViewModel: LandingPageViewModel by viewModels()
    private lateinit var adapter: ClassAdapter
    private var classList = mutableListOf<ClassData>()
    private lateinit var mRef: DatabaseReference
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var binding: FragmentLandingPageBinding

    private val actionDispatcher = ActionDispatcher(object : ActionExecutor {
        override fun dispatch(actionWrapper: ActionWrapper?) {
            val classItem = actionWrapper?.payload as ClassData
            val args = bundleOf(
                FragmentLandingPageARGS.KEY.CLASS_ID to classItem.id,
                FragmentLandingPageARGS.KEY.OWNER_ID to classItem.ownerId
            )
//            requestNavigate(R.id.fragmentClassDetail, args)
            val intent = Intent(context, ActivityClassDetailHost::class.java)
            intent.putExtra("data", args)
            startActivity(intent)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLandingPageBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initData() {
        super.initData()
        adapter = ClassAdapter(actionDispatcher, classList)
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
    }

    override fun initView() {
        super.initView()
        val layoutManager = GridLayoutManager(context, 2)
        rv_class.layoutManager = layoutManager
        rv_class.adapter = adapter

        landingPageViewModel.getUserInfo(firebaseUser.uid)
        landingPageViewModel.getAllClassIdByUserId(firebaseUser.uid)
        initDrawer()
    }

    private fun initDrawer() {
        binding.navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.drawer_message -> {
                    startActivity(Intent(context, ActivityChat::class.java))
                }

                R.id.drawer_signout -> {
                    FirebaseAuth.getInstance().signOut()
                    startActivity(Intent(activity, LoginActivity::class.java))
                    activity?.finish()
                }
            }
            true
        }
    }

    override fun initListener() {
        super.initListener()
        showLoading()
        bt_create_class.setOnClickListener {
            FragmentCreateClass().show(childFragmentManager, null)
        }

        bt_join.setOnClickListener {
            FragmentJoinClass().show(childFragmentManager, null)
        }

        bt_drawer.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }

        binding.ivAvatar.setOnClickListener {
//            val action = FragmentLandingPageDirections.actionFragmentLandingPageToFragmentProfile()
//
//            findNavController().navigate(action)

            requestNavigate(
                R.id.fragmentProfile, bundleOf(
                    FragmentProfileARGS.KEY.USER_ID to firebaseUser.uid
                )
            )
        }

        lifecycleScope.launch {
            landingPageViewModel.userInfo.collect {
                if (it.status == ResultWrapper.Status.SUCCESS && it.data != null) {
                    binding.tvFullName.text = it.data.fullname
                    binding.tvUserName.text = "@" + it.data.username
                    if (it.data.imageUrl.isNotEmpty()) {
                        Glide.with(requireContext())
                            .load(it.data.imageUrl)
                            .into(binding.ivAvatar)
                        Glide.with(requireContext())
                            .load(it.data.imageUrl)
                            .into(
                                binding.navView.getHeaderView(0)
                                    .findViewById<ImageView>(R.id.header_iv_drawer_avatar)
                            )
                    }

                    binding.navView.getHeaderView(0)
                        .findViewById<TextView>(R.id.header_tv_full_name).text = it.data.fullname
                    binding.navView.getHeaderView(0)
                        .findViewById<TextView>(R.id.header_tv_user_name).text =
                        "@${it.data.username}"
                }
            }
        }

        lifecycleScope.launch {
            landingPageViewModel.classDataListFlow.collect { classDataList ->
                if (classDataList.isEmpty())
                    binding.tvNoClass.show()
                else
                    binding.tvNoClass.gone()
                classList.run {
                    clear()
                    addAll(classDataList)
                }
                classList.sortedBy { it.createdDate }
                classList.reversed()

                launch(Dispatchers.Main) {
                    adapter.notifyDataSetChanged()
                }
                hideLoading()
            }
        }
    }


}