package com.example.pclassmanagement.ui.profile

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.Contribution
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.StorageRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FragmentProfileViewModel @Inject constructor(
    private val contentRepository: ContentRepository,
    private val userRepository: UserRepository,
    private val storageRepository: StorageRepository
) : ViewModel() {

    private val _userInfo =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val userInfo: SharedFlow<ResultWrapper<User>>
        get() = _userInfo

    private val _updateUserInfo =
        MutableSharedFlow<ResultWrapper<Boolean>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val updateUserInfo: SharedFlow<ResultWrapper<Boolean>>
        get() = _updateUserInfo

    private val _uploadUserAvatarStatus =
        MutableSharedFlow<String>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val uploadUserAvatarStatus: SharedFlow<String>
        get() = _uploadUserAvatarStatus

    private val _uploadUserBackgroundStatus =
        MutableSharedFlow<String>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val uploadUserBackgroundStatus: SharedFlow<String>
        get() = _uploadUserBackgroundStatus

    private val _contributionList =
        MutableSharedFlow<List<Contribution>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val contributionList: SharedFlow<List<Contribution>>
        get() = _contributionList

    private val _classIdListByUser =
        MutableSharedFlow<List<String>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val classIdListByUser: SharedFlow<List<String>>
        get() = _classIdListByUser

    fun getUserInfo(userId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(userId).collect {
                _userInfo.tryEmit(it)
            }
        }
    }

    fun getClassCount(userId: String) {
        viewModelScope.launch {
            contentRepository.getAllClassByUserId(userId).collect { classIdList ->
                if(classIdList.status == ResultWrapper.Status.SUCCESS){
                    classIdList.data?.let { _classIdListByUser.tryEmit(it) }
                }
            }
        }
    }

    fun getAllContributionByUserId(userId: String){
        viewModelScope.launch {
            contentRepository.getAllContributionByUserId(userId).collect { contributionListResponse ->
                if(contributionListResponse.status == ResultWrapper.Status.SUCCESS){
                    contributionListResponse.data?.let { _contributionList.tryEmit(it) }
                }

//                if(classIdList.status == ResultWrapper.Status.SUCCESS){
//                    classIdList.data?.let { _classIdListByUser.tryEmit(it) }
//                }
            }
        }
    }

    fun updateUserInfo(value: HashMap<String, Any>) {
        viewModelScope.launch {
            userRepository.updateUserInfo(value).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    _updateUserInfo.tryEmit(it)
                }
            }
        }
    }

    fun uploadAvatarImage(uri: Uri) {
        viewModelScope.launch {
            storageRepository.uploadAvatarImage(uri).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
//                    _uploadUserAvatarStatus.tryEmit(it.data.toString())
                    updateUserInfo(hashMapOf("imageUrl" to it.data.toString()))
                }
            }
        }
    }

    fun uploadBackgroundImage(uri: Uri) {
        viewModelScope.launch {
            storageRepository.uploadABackgroundImage(uri).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
//                    _uploadUserAvatarStatus.tryEmit(it.data.toString())
                    updateUserInfo(hashMapOf("background" to it.data.toString()))
                }
            }
        }
    }

}