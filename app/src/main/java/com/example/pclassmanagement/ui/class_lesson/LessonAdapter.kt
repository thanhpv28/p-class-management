package com.example.pclassmanagement.ui.class_lesson

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.Action
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.model.ActionWrapper
import com.example.pclassmanagement.data.model.Lesson
import com.example.pclassmanagement.data.model.StudentLesson
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.example.pclassmanagement.ui.base.ActionDispatcher
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.item_assignment.view.tv_assignment_title
import kotlinx.android.synthetic.main.item_lesson.view.*
import java.util.*

class LessonAdapter(
    private val actionDispatcher: ActionDispatcher,
    private val data: List<Lesson>,
    private val ownerId: String,
    private val uid: String,
    private val context: Context,
) :
    RecyclerView.Adapter<LessonAdapter.CommentViewHolder>() {

    private val assignmentUserRef =
        FirebaseDatabase.getInstance().getReference(Table.ASSIGNMENT_USER)
    private val lessonRef =
        FirebaseDatabase.getInstance().getReference(Table.LESSON)
    private val studentLessonRef =
        FirebaseDatabase.getInstance().getReference(Table.STUDENT_LESSON)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            actionDispatcher,
            LayoutInflater.from(parent.context).inflate(R.layout.item_lesson, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bindView(data[position], context = context)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class CommentViewHolder(private val actionDispatcher: ActionDispatcher, itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        fun bindView(item: Lesson, context: Context) {
            val status =
                if (item.finished) "Đã kết thúc"
                else if (item.inAttendance) "Đang điểm danh"
                else if (item.started) "Đang diễn ra"
                else "Chưa diễn ra"


            if (ownerId != uid) {
                if (item.finished) {
                    itemView.bt_attending.gone()

                } else if(item.started || item.inAttendance) {
                    itemView.bt_attending.show()
                } else{
                    itemView.tv_attendance.gone()
                }
            } else {
                if (item.finished) {
                    itemView.bt_finish_lesson.gone()
                    itemView.bt_lesson_statistic.show()
                } else if (item.inAttendance) {
                    itemView.bt_finish_attending_action.show()
                    itemView.bt_finish_lesson.gone()
                    itemView.bt_start_lesson.gone()
                } else if (item.started) {
                    itemView.bt_finish_lesson.show()
                    itemView.bt_finish_attending_action.gone()
                    itemView.bt_start_lesson.gone()
                } else {
                    itemView.bt_start_lesson.show()
                    itemView.bt_finish_lesson.gone()
                    itemView.bt_finish_attending_action.gone()
                }
            }

            itemView.tv_assignment_title.text = item.title
            itemView.tv_lesson_date.text =
                Utils.getDateFormatted(item.startDate) + " - " + status

            if (uid != ownerId)
                studentLessonRef.child(item.id)
                    .addValueEventListener(object : ValueEventListener {
                        @RequiresApi(Build.VERSION_CODES.M)
                        override fun onDataChange(snapshot: DataSnapshot) {
                            var isAttendance = false
                            snapshot.children.forEach {
                                val studentLesson = it.getValue(StudentLesson::class.java)
                                if (studentLesson?.userId == uid) {
                                    if (studentLesson.late) {
                                        itemView.tv_attendance.text = "Muộn"
                                        itemView.tv_attendance.setTextColor(
                                            context.resources.getColor(
                                                R.color.orange,
                                                context.theme
                                            )
                                        )
                                    } else {
                                        itemView.tv_attendance.text = "Có mặt"
                                        itemView.tv_attendance.setTextColor(
                                            context.resources.getColor(
                                                R.color.color_success,
                                                context.theme
                                            )
                                        )
                                    }
                                    isAttendance = true
                                    itemView.bt_attending.gone()
                                    if(item.finished)
                                        itemView.bt_lesson_feedback.show()
                                }
                            }
                            if (!isAttendance) {
                                itemView.tv_attendance.text = "Vắng mặt"
                                itemView.tv_attendance.setTextColor(
                                    context.resources.getColor(
                                        R.color.color_error,
                                        context.theme
                                    )
                                )
                            }

                        }

                        override fun onCancelled(error: DatabaseError) {
                            TODO("Not yet implemented")
                        }

                    })

            // who is the commenter
//            userRef.child(FirebaseAuth.getInstance().currentUser?.uid.toString()).addListenerForSingleValueEvent(
//                object : ValueEventListener {
//                    override fun onDataChange(snapshot: DataSnapshot) {
//                        val user = snapshot.getValue(User::class.java)
//                        if (user?.id == item.userId) {
//                            if (user.imageUrl.isNotEmpty()) {
//                                Glide.with(itemView.context)
//                                    .load(user.imageUrl)
//                                    .into(itemView.iv_comment_user)
//                            }
//                            itemView.tv_comment_username.text = user.username
//                        }
//
//
//                    }
//
//                    override fun onCancelled(error: DatabaseError) {
//                        TODO("Not yet implemented")
//                    }
//
//                }
//            )

            itemView.bt_start_lesson.setOnClickListener {
                lessonRef.child(item.id).child("started").setValue(true)
                lessonRef.child(item.id).child("startDate").setValue(Date().toString())
                lessonRef.child(item.id).child("inAttendance").setValue(true)
            }
            itemView.bt_finish_attending_action.setOnClickListener {
                lessonRef.child(item.id).child("inAttendance").setValue(false)
            }

            itemView.bt_finish_lesson.setOnClickListener {
                lessonRef.child(item.id).child("finished").setValue(true)
                lessonRef.child(item.id).child("finishedDate").setValue(Date().toString())
            }

            itemView.bt_attending.setOnClickListener {
                studentLessonRef.child(item.id).push().setValue(
                    hashMapOf(
                        "userId" to uid,
                        "lessonId" to item.id,
                        "attended" to true,
                        "attendDate" to Date().toString(),
                        "late" to !item.inAttendance
                    )
                ).addOnCompleteListener {
                    if (it.isSuccessful) {
                        actionDispatcher.dispatch(
                            actionWrapper = ActionWrapper(
                                Action.Item.SHOW_TOAST,
                                "Điểm danh thành công!"
                            )
                        )
                        itemView.bt_attending.gone()
                    }
                }

                // add contribution point when attending
                if(item.inAttendance)
                    actionDispatcher.dispatch(ActionWrapper(name = Action.Item.ATTENDANCE_IN_TIME))
                else
                    actionDispatcher.dispatch(ActionWrapper(name = Action.Item.ATTENDANCE_LATE))
            }

            itemView.bt_lesson_statistic.setOnClickListener {
                actionDispatcher.dispatch(
                    actionWrapper = ActionWrapper(
                        name = Action.Item.LESSON_VIEW_STATISTIC,
                        payload = item
                    )
                )
            }

            itemView.bt_lesson_feedback.setOnClickListener {
                actionDispatcher.dispatch(
                    actionWrapper = ActionWrapper(
                        name = Action.Item.LESSON_FEEDBACK,
                        payload = item
                    )
                )
            }

            //                    hashMapOf(
//                            "id" to ,
//                            "title" to "Buổi 1: Giới thiệu chung",
//                            "detail" to  "Giới thiệu về IOS cơ bản",
//                            "startDate" to  "22/10/2022",
//                            "classId" to  "DjsJv7QNT1SBwztSjaJX",
//                            "teacherId" to  "",
//                            "isStart" to  false,
//                            "isInAttendance" to  false,
//                            "isFinished" to  false
//
//                    )
        }
    }

}

