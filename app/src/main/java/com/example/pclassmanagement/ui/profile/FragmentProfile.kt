package com.example.pclassmanagement.ui.profile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.pclassmanagement.R
import com.example.pclassmanagement.databinding.FragmentProfileBinding
import com.example.pclassmanagement.data.FragmentProfileARGS
import com.example.pclassmanagement.data.Gender
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.shared.ext.canFocus
import com.example.pclassmanagement.shared.ext.canNotFocus
import com.example.pclassmanagement.shared.ext.gone
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.ui.base.BaseFragment
import com.example.pclassmanagement.ui.image_viewer.ActivityImageViewer
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


@AndroidEntryPoint
class FragmentProfile : BaseFragment() {

    private var userId = ""
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var binding: FragmentProfileBinding
    private val profileViewModel: FragmentProfileViewModel by viewModels()
    private var avatarUrl = ""
    private var backgroundUrl = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun initData() {
        super.initData()
        userId = arguments?.getString(FragmentProfileARGS.KEY.USER_ID, "").toString()
    }

    override fun initView() {
        super.initView()
        profileViewModel.getUserInfo(userId)
        profileViewModel.getClassCount(userId)
        profileViewModel.getAllContributionByUserId(userId)
        showLoading()

        // view others
        if (userId != firebaseAuth.uid) {
            binding.btMessage.show()
            binding.btEdit.gone()
        }
    }

    override fun initListener() {
        super.initListener()
        lifecycleScope.launch {
            profileViewModel.userInfo.collect {
                hideLoading()
                val userInfo = it.data

                if (it.status == ResultWrapper.Status.SUCCESS) {
                    avatarUrl = it.data?.imageUrl.toString()
                    backgroundUrl = it.data?.background.toString()
                    Glide.with(requireContext())
                        .load(it.data?.imageUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(binding.ivAvatar)

                    Glide.with(requireContext())
                        .load(it.data?.background)
                        .placeholder(R.drawable.placeholder)
                        .into(binding.ivBackground)
                    binding.tvFullName.text = userInfo?.fullname
                    binding.tvUserFullName.setText(userInfo?.fullname)
                    binding.tvUserCode.setText(userInfo?.studentCode)
                    binding.tvUserClass.setText(userInfo?.studentClass)
                    binding.tvUserIdentity.setText(userInfo?.naturalId)
                    binding.tvUserPhone.setText(userInfo?.phoneNumber)
                    binding.tvUserGender.setText(
                        when (userInfo?.gender) {
                            Gender.MALE -> "Nam"
                            Gender.FEMALE -> "Nữ"
                            else -> "Khác"
                        }
                    )
                    binding.tvUserAddress.setText(userInfo?.address)
                }
            }
        }

        lifecycleScope.launch {
            profileViewModel.updateUserInfo.collectLatest {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    showToast("Cập nhật thông tin thành công!")
                    hideLoading()
                }
            }
        }

        // get all classes which this user joined
        lifecycleScope.launch {
            profileViewModel.classIdListByUser.collectLatest {
                binding.tvClassCount.text = it.size.toString()
            }
        }

        // listen get all contributions
        lifecycleScope.launch {
            profileViewModel.contributionList.collectLatest {
                binding.tvContributedScore.text = it.size.toString()
            }
        }


        // change avatar
        binding.ivAvatar.setOnClickListener { view ->
            val popupMenu = PopupMenu(requireContext(), view, Gravity.BOTTOM)
            popupMenu.inflate(R.menu.avatar_menu)
            popupMenu.show()
            popupMenu.setOnMenuItemClickListener { menuItem ->
                if (menuItem.itemId == R.id.change) {
                    openGalleryToChooseImage(AVATAR_REQUEST_CODE)
                } else if (menuItem.itemId == R.id.view_avatar) {
                    val intent = Intent(requireContext(), ActivityImageViewer::class.java)
                    intent.putExtra("src", avatarUrl)
                    startActivity(intent)
                }
                false
            }
        }

        binding.btEdit.setOnClickListener {
            binding.btEdit.gone()
            binding.tvUserFullName.canFocus()
            binding.tvUserCode.canFocus()
            binding.tvUserClass.canFocus()
            binding.tvUserIdentity.canFocus()
            binding.tvUserPhone.canFocus()
            binding.tvUserGender.canFocus()
            binding.tvUserAddress.canFocus()
//            profileViewModel.updateUserInfo(userId, value)
        }

        binding.ivBackground.setOnClickListener {
            val intent = Intent(requireContext(), ActivityImageViewer::class.java)
            intent.putExtra("src", backgroundUrl)
            startActivity(intent)
        }

        binding.btUpdate.setOnClickListener {
            showLoading()
            binding.btEdit.show()
            binding.tvUserFullName.canNotFocus()
            binding.tvUserCode.canNotFocus()
            binding.tvUserClass.canNotFocus()
            binding.tvUserIdentity.canNotFocus()
            binding.tvUserPhone.canNotFocus()
            binding.tvUserGender.canNotFocus()
            binding.tvUserAddress.canNotFocus()

            val value = hashMapOf<String, Any>(
                "fullname" to binding.tvUserFullName.text.toString().trim(),
                "studentCode" to binding.tvUserCode.text.toString().trim(),
                "studentClass" to binding.tvUserClass.text.toString().trim(),
                "naturalId" to binding.tvUserIdentity.text.toString().trim(),
                "phoneNumber" to binding.tvUserPhone.text.toString().trim(),
                "gender" to Gender.MALE,
                "address" to binding.tvUserAddress.text.toString().trim(),
            )

            profileViewModel.updateUserInfo(value)

//            hideLoading()
        }

        // change background
        binding.btChangeBackground.setOnClickListener {
            openGalleryToChooseImage(BACKGROUND_REQUEST_CODE)
        }
    }

    private fun openGalleryToChooseImage(requestCode: Int) {
        ImagePicker.with(this)
            .galleryOnly()
            .start(requestCode)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                val uri: Uri = data?.data!!
                showLoading()
                when (requestCode) {
                    AVATAR_REQUEST_CODE -> profileViewModel.uploadAvatarImage(uri)
                    BACKGROUND_REQUEST_CODE -> profileViewModel.uploadBackgroundImage(uri)
                }
            }
            ImagePicker.RESULT_ERROR -> {
            }
            else -> {
            }
        }
    }

    companion object {
        private const val AVATAR_REQUEST_CODE = 1
        private const val BACKGROUND_REQUEST_CODE = 2
    }

}