package com.example.pclassmanagement.ui.class_detail

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.databinding.FragmentClassUtilityBinding
import com.example.pclassmanagement.ui.leader_board.ActivityLeaderBoard
import com.example.pclassmanagement.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ClassUtilityFragment : BaseFragment() {

    private val classViewModel: ClassViewModel by viewModels()
    lateinit var classId: String
    lateinit var ownerId: String

    private lateinit var binding: FragmentClassUtilityBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentClassUtilityBinding.inflate(inflater)
        return binding.root
    }

    override fun initData() {
        classId = arguments?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        ownerId = arguments?.getString(FragmentLandingPageARGS.KEY.OWNER_ID).toString()
        super.initData()
    }

    override fun initView() {
        super.initView()
        // get class detail
        classViewModel.getClassDetail(classId)
    }

    override fun initListener() {
        super.initListener()

        binding.btCopy.setOnClickListener {
            val clipBoard = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE)
            val clip = ClipData.newPlainText("label",binding.tvClassCode.text)
            (clipBoard as ClipboardManager).setPrimaryClip(clip)
            showToast("Sao chép mã lớp thành công")
        }

        binding.btStatistic.setOnClickListener {

        }

        binding.btLeaderBoard.setOnClickListener {
            val intent = Intent(requireContext(), ActivityLeaderBoard::class.java)
            intent.putExtra("classId", classId)
            startActivity(intent)
        }
        lifecycleScope.launch {
            classViewModel.classDetail.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    binding.tvClassCode.text =
                        if (it.data?.invitationCode.isNullOrEmpty()) "Loading..." else it.data?.invitationCode

                }
            }
        }
    }


}