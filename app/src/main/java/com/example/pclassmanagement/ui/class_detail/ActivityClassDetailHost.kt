package com.example.pclassmanagement.ui.class_detail

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.FragmentLandingPageARGS
import com.example.pclassmanagement.ui.base.BaseActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ActivityClassDetailHost : BaseActivity() {

    private var classId = ""
    private var ownerId = ""
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController
    private var currentBottomMenuId = R.id.bottom_menu_home
    private var currentFragmentId = R.id.fragmentHome2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class_detail_host)
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.class_detail_host) as NavHostFragment
        navController = navHostFragment.navController

        setUpBottomNavigation()

        val args = intent.getBundleExtra("data")
        classId = args?.getString(FragmentLandingPageARGS.KEY.CLASS_ID).toString()
        ownerId = args?.getString(FragmentLandingPageARGS.KEY.OWNER_ID).toString()

        val startArgument = bundleOf(
            FragmentLandingPageARGS.KEY.CLASS_ID to classId,
            FragmentLandingPageARGS.KEY.OWNER_ID to ownerId
        )
        navController.setGraph(R.navigation.class_detail_graph, startArgument)
    }

    private fun navReplace(fragmentGraphId: Int){
        val startArgument = bundleOf(
            FragmentLandingPageARGS.KEY.CLASS_ID to classId,
            FragmentLandingPageARGS.KEY.OWNER_ID to ownerId
        )
        navController.navigate(
            fragmentGraphId,
            startArgument,
            NavOptions.Builder().setPopUpTo(currentFragmentId, true).build()
        )
        currentFragmentId = fragmentGraphId
    }

    private fun setUpBottomNavigation() {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav)

        bottomNavigationView.setOnItemSelectedListener {
            if (it.itemId == currentBottomMenuId) return@setOnItemSelectedListener true
            currentBottomMenuId = it.itemId

            when (it.itemId) {
                R.id.bottom_menu_home -> {
                    navReplace(R.id.fragmentHome2)
                }
                R.id.bottom_menu_assignment -> {
                    navReplace(R.id.fragmentAssignment)
                }
                R.id.bottom_menu_lesson -> {
                    navReplace(R.id.fragmentLesson)
                }
                R.id.bottom_menu_notification -> {
                    navReplace(R.id.fragmentClassNotification)
                }
                R.id.bottom_menu_utility -> {
                    navReplace(R.id.classUtilityFragment)
                }
            }
            true
        }


    }

}