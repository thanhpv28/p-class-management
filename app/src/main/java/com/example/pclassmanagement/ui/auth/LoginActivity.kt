package com.example.pclassmanagement.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.example.pclassmanagement.MainActivity
import com.example.pclassmanagement.R
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.databinding.ActivityLoginBinding
import com.example.pclassmanagement.ui.base.BaseActivity
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginActivity : BaseActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var binding: ActivityLoginBinding
    private val authViewModel: AuthViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        if (mAuth.currentUser != null)
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
    }

    override fun initListener() {
        super.initListener()

        authViewModel.getUserSaved().let {
            binding.etUsername.setText(it.email)
            binding.etPassword.setText(it.password)
            binding.cbRemember.isChecked = it.remember
        }

        binding.btLogin.setOnClickListener {
            val email: String = binding.etUsername.text.toString()
            val pass: String = binding.etPassword.text.toString()

            if (email.isEmpty() || pass.isEmpty()) {
                showToast("Vui lòng điền đầy đủ Email và Password")
                return@setOnClickListener
            } else {
                showLoading()
                authViewModel.login(email, pass, binding.cbRemember.isChecked)
            }
        }

        bt_register.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
        }

        lifecycleScope.launch {
            authViewModel.loginState.collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                } else
                    it.message?.let { it1 -> showToast(it1) }
                hideLoading()
            }
        }
    }

}