package com.example.pclassmanagement.ui.landing_page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.log
import com.example.pclassmanagement.data.model.ClassData
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.data.repository.ContentRepository
import com.example.pclassmanagement.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LandingPageViewModel @Inject constructor(
    private val contentRepository: ContentRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    val classDataList = mutableListOf<ClassData>()

    private val _classDataListFlow =
        MutableSharedFlow<List<ClassData>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val classDataListFlow: SharedFlow<List<ClassData>>
        get() = _classDataListFlow

    private val _joinClassSuccess =
        MutableSharedFlow<ResultWrapper<Boolean>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val joinClassSuccess: SharedFlow<ResultWrapper<Boolean>>
        get() = _joinClassSuccess

    private val _createClassSuccess =
        MutableSharedFlow<ResultWrapper<Boolean>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val createClassSuccess: SharedFlow<ResultWrapper<Boolean>>
        get() = _createClassSuccess

    private val _userInfo =
        MutableSharedFlow<ResultWrapper<User>>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_LATEST
        )

    val userInfo: SharedFlow<ResultWrapper<User>>
        get() = _userInfo

    fun getUserInfo(userId: String) {
        viewModelScope.launch {
            userRepository.getUserInfo(userId).collect {
                _userInfo.tryEmit(it)
            }
        }
    }

    fun getAllClassIdByUserId(userId: String) {
        viewModelScope.launch {
            contentRepository.getAllClassByUserId(userId).collect { classIdList ->
                if (classIdList.status == ResultWrapper.Status.SUCCESS) {
                    classDataList.clear()
                    if (classIdList.data?.isEmpty() == true) {
                        _classDataListFlow.tryEmit(mutableListOf())
                    } else {
                        classIdList.data?.forEach { classId ->
                            viewModelScope.launch {
                                contentRepository.getClassDetail(classId).collect { classData ->
                                    classData.data?.let {
                                        classDataList.add(it)
                                        log(classData.data.id,"get detail")
                                    }
                                }
                            }
                        }
                        log("before delay","get detail")
                        delay(1000)
                        _classDataListFlow.tryEmit(classDataList.reversed())
                        log("after delay","get detail")

                    }
                }
            }
        }
    }

    fun joinClass(userId: String, classCode: String) {
        viewModelScope.launch {
            contentRepository.getClassByCode(classCode).collect {
                if (it.status == ResultWrapper.Status.SUCCESS) {
                    contentRepository.joinClass(userId, it.data!!.id).collect { joinClass ->
                        _joinClassSuccess.tryEmit(joinClass)
                    }
                } else {
                    _joinClassSuccess.tryEmit(ResultWrapper.error(it.message.toString()))
                }
            }

        }
    }

    fun createClass(className: String, classDes: String) {
        viewModelScope.launch {
            contentRepository.createClass(className, classDes).collect { it ->
                if (it.status == ResultWrapper.Status.SUCCESS) {

                    // create User_Class table, because it's many to many relationship
                    it.data?.let { it1 ->
                        contentRepository.joinClass(it1.userId, it1.classId).collect { joinState ->
                            _createClassSuccess.tryEmit(joinState)
                        }
                    }
                }
            }
        }
    }

}