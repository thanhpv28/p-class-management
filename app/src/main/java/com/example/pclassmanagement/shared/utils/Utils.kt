package com.example.pclassmanagement.shared.utils

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.provider.OpenableColumns
import androidx.annotation.RequiresApi
import com.example.pclassmanagement.data.log
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


object Utils {

    fun createId(length: Int = 20): String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        val id = (1..length)
            .map { "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".random() }
            .joinToString("")
        return id
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateFromMillisecond(millis: Long): String {
        val c = Calendar.getInstance()
        c.timeInMillis = millis
        val dateFormat = SimpleDateFormat("HH:mm - dd/MM/yyyy")
        return dateFormat.format(c.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateFormatted(input: String): String {
        return try {
            val df = SimpleDateFormat("dd/MM/yyyy")
            val df2 = SimpleDateFormat("dd/MM/yyyy")
            val dateFromInput = Date(input)
//            df.parse(input)?.let { df2.format(it) }.toString()
            df2.format(dateFromInput)
        } catch (e: java.lang.Exception) {
            input
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getDateTimeNowString(): String {
        return LocalDateTime.now().toString()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SimpleDateFormat")
    fun getDateTimeFormatted(input: String): String {
        return try {
            val df = DateTimeFormatter.ofPattern("hh:mm:ss dd/MM/yyyy")
            val localDateFromInput = LocalDate.parse(input)
            localDateFromInput.format(df)
        } catch (e: java.lang.Exception) {
            log(e.toString())
            input
        }
    }

    fun getFileName(context: Context, uri: Uri): String? {
        var result: String? = null
        if (uri.scheme.equals("content")) {
            val cursor: Cursor? = context.contentResolver.query(uri, null, null, null, null)
            cursor.use { cursor ->
                if (cursor != null && cursor.moveToFirst()) {
                    val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    result = cursor.getString(nameIndex)
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result?.lastIndexOf('/')
            if (cut != -1) {
                if (cut != null) {
                    result = result?.substring(cut + 1)
                }
            }
        }
        return result
    }

}