package com.example.pclassmanagement.shared.ext

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

fun <T> Flow<T>.backgroundSub(): Flow<T> {
    return flowOn(Dispatchers.IO)
}