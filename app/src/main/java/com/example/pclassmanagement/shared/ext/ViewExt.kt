package com.example.pclassmanagement.shared.ext

import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import com.example.pclassmanagement.R

fun TextView.setMarquee(){
    isSelected = true
    ellipsize = TextUtils.TruncateAt.MARQUEE
    isSingleLine = true
}

fun TextView.disableMarquee(){
    isSelected = false
    ellipsize = null
//    isSingleLine = false
}

fun View.show(){
    visibility = View.VISIBLE
}

fun View.gone(){
    visibility = View.GONE
}

fun EditText.canFocus(){
    isFocusable = true
    isFocusableInTouchMode = true
    background = AppCompatResources.getDrawable(context, R.drawable.bg_primary_edt_underbar )
}

fun EditText.canNotFocus(){
    isFocusable = false
    isFocusableInTouchMode = false
    background = null

}