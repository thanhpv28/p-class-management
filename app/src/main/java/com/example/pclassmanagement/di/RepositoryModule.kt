package com.example.pclassmanagement.di

import com.example.pclassmanagement.data.local.LocalDataAccess
import com.example.pclassmanagement.data.local.SharedPreferencesHelper
import com.example.pclassmanagement.data.repository.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository

    @Binds
    abstract fun provideContentRepository(contentRepository: ContentRepositoryImpl): ContentRepository

    @Binds
    abstract fun provideStorageRepository(storageRepository: StorageRepositoryImpl): StorageRepository

    @Binds
    abstract fun provideLocalDataAccess(sharedPreferencesHelper: SharedPreferencesHelper): LocalDataAccess
}