package com.example.pclassmanagement.data.model

data class Note(
    val id: String,
    val assignmentId: String,
    val userId: String,
    val createdDate: Long,
    val note: String,
    val createBy: String
): java.io.Serializable{
    constructor(): this("","","",0,"","")
}
