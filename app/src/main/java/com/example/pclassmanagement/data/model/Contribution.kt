package com.example.pclassmanagement.data.model

data class UserContribution(
    var userId: String,
    var contributionList: List<Contribution>
)

data class UserContributionData(
    var userContribution: UserContribution,
    var userFullName: String,
    var userCode: String,
    var totalPoint: Long
)

data class Contribution(
    val id: String,
    val userId: String,
    val classId: String,
    val type: Long,
    val point: Long
)
