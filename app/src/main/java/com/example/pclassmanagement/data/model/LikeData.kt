package com.example.pclassmanagement.data.model

data class LikeData(
    val postId: String,
    val userId: String,
){
    constructor() : this("", "")
}
