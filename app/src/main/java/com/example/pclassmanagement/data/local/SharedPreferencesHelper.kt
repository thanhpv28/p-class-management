package com.example.pclassmanagement.data.local

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.pclassmanagement.data.SharedPreferencesKey
import com.example.pclassmanagement.data.model.UserInfoSaved
import javax.inject.Inject

class SharedPreferencesHelper @Inject constructor(private val sharedPreferences: SharedPreferences) :
    LocalDataAccess {

    override fun saveInfo(userId: String, email: String, password: String, remember: Boolean) {
        sharedPreferences.edit {
            putString(SharedPreferencesKey.USER_ID, userId)
            putString(SharedPreferencesKey.EMAIL, email)
            putString(SharedPreferencesKey.PASSWORD, password)
            putBoolean(SharedPreferencesKey.REMEMBER_INFO, remember)
        }
    }

    override fun getUserId(): String {
        return sharedPreferences.getString(SharedPreferencesKey.USER_ID, "").toString()
    }

    override fun getEmail(): String {
        return sharedPreferences.getString(SharedPreferencesKey.EMAIL, "").toString()
    }

    override fun getPassword(): String {
        return sharedPreferences.getString(SharedPreferencesKey.PASSWORD, "").toString()
    }

    override fun getRemember(): Boolean {
        return sharedPreferences.getBoolean(SharedPreferencesKey.REMEMBER_INFO, false)
    }

    override fun getUserInfo(): UserInfoSaved {
        return UserInfoSaved(
            userId = getUserId(),
            email = getEmail(),
            password = getPassword(),
            remember = getRemember(),
        )
    }

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }
}