package com.example.pclassmanagement.data.repository

import android.os.Build
import androidx.annotation.RequiresApi
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.log
import com.example.pclassmanagement.data.model.*
import com.example.pclassmanagement.shared.ext.backgroundSub
import com.example.pclassmanagement.shared.ext.show
import com.example.pclassmanagement.shared.utils.Utils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.item_submission.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.util.*
import javax.inject.Inject

class ContentRepositoryImpl @Inject constructor() : ContentRepository {

    private val classRef = FirebaseDatabase.getInstance().getReference(Table.CLASS)
    private val postRef = FirebaseDatabase.getInstance().getReference(Table.POST)
    private val commentRef = FirebaseDatabase.getInstance().getReference(Table.COMMENT)
    private val likeRef = FirebaseDatabase.getInstance().getReference(Table.LIKE)
    private val contributionRef = FirebaseDatabase.getInstance().getReference(Table.CONTRIBUTION)
    private val userClassRef = FirebaseDatabase.getInstance().getReference(Table.USER_CLASS)
    private val auth = FirebaseAuth.getInstance()
    private val currentUserId = FirebaseAuth.getInstance().uid
    private val studentLessonRef = FirebaseDatabase.getInstance().getReference(Table.STUDENT_LESSON)
    private val submissionRef = FirebaseDatabase.getInstance().getReference(Table.Submission)
    private val noteRef = FirebaseDatabase.getInstance().getReference(Table.NOTE)
    private val markRef = FirebaseDatabase.getInstance().getReference(Table.MARK)
    private val resultRef = FirebaseDatabase.getInstance().getReference(Table.RESULT)
    private val markTypeRef = FirebaseDatabase.getInstance().getReference(Table.MARK_TYPE)
    private val feedbackLessonRef =
        FirebaseDatabase.getInstance().getReference(Table.FEEDBACK_LESSON)

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getClassByCode(classCode: String): Flow<ResultWrapper<ClassData>> {
        return callbackFlow<ResultWrapper<ClassData>> {
            var classAvailable = false
            classRef.get().addOnSuccessListener { classList ->
                classList.children.forEach {
                    val classData = it.getValue(ClassData::class.java)
                    if (classData?.invitationCode == classCode) {
                        classAvailable = true
                        trySendBlocking(ResultWrapper.success(classData))
                        return@addOnSuccessListener
                    }
                }
                if (!classAvailable) trySendBlocking(ResultWrapper.error("Không tìm thấy lớp học"))
            }

            awaitClose { }
        }.backgroundSub()
    }

    @ExperimentalCoroutinesApi
    override suspend fun joinClass(userId: String, classId: String)
            : Flow<ResultWrapper<Boolean>> = callbackFlow<ResultWrapper<Boolean>> {
        val userClass = hashMapOf(
            "userId" to userId,
            "classId" to classId,
        )

        val onCompleteListener = OnCompleteListener<Void> {
            if (it.isSuccessful) {
                this@callbackFlow.trySendBlocking(ResultWrapper.success(true))
            } else {
            }
        }
        userClassRef.push().setValue(userClass).addOnCompleteListener(onCompleteListener)
        awaitClose {

        }
    }.backgroundSub()

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun createClass(
        className: String,
        classDes: String
    ): Flow<ResultWrapper<UserClass>> {
        return callbackFlow<ResultWrapper<UserClass>> {
            val owner = auth.currentUser?.uid.toString()
            val classId = classRef.push().key ?: Utils.createId()
            val invitationCode = Utils.createId(7)
            val myClass = hashMapOf(
                "id" to classId,
                "invitationCode" to invitationCode,
                "name" to className,
                "des" to classDes,
                "ownerId" to owner,
                "schedule" to "default",
                "thumb" to "https://img.freepik.com/premium-photo/empty-white-classroom-background-with-green-chalkboard-table-seat-wooden-floor_10307-1501.jpg?w=2000",
                "createdDate" to System.currentTimeMillis().toString()
            )
            classRef.child(classId).setValue(myClass).addOnCompleteListener {
                if (it.isSuccessful) {
                    trySendBlocking(ResultWrapper.success(UserClass(classId, owner)))
                }
            }.addOnFailureListener {
                trySendBlocking(ResultWrapper.error(""))
            }

            awaitClose { }
        }.backgroundSub()
    }

    override suspend fun getAllClassByUserId(userId: String): Flow<ResultWrapper<List<String>>> {
        return callbackFlow {
            val classIdList = mutableListOf<String>()
            userClassRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    classIdList.clear()
                    snapshot.children.forEach {
                        val userClass = it.getValue(UserClass::class.java)
                        if (userClass != null) {
                            if (userClass.userId == userId)
                                classIdList.add(userClass.classId)
                        }
                    }
                    trySendBlocking(ResultWrapper.success(classIdList))
                }

                override fun onCancelled(error: DatabaseError) {}


            })

            awaitClose {}
        }
    }

//    override suspend fun getAllClassDetailByUserId(userId: String): Flow<ResultWrapper<List<ClassData>>> {
//        return callbackFlow {
//            val classIdList = mutableListOf<String>()
//            userClassRef.addValueEventListener(object : ValueEventListener {
//                override fun onDataChange(snapshot: DataSnapshot) {
//                    classIdList.clear()
//                    snapshot.children.forEach {
//                        val userClass = it.getValue(UserClass::class.java)
//                        if (userClass != null) {
//                            if (userClass.userId == userId)
//                                classIdList.add(userClass.classId)
//                        }
//                    }
//                    trySendBlocking(ResultWrapper.success(classIdList))
//                }
//
//                override fun onCancelled(error: DatabaseError) {}
//
//
//            })
//
//            awaitClose {}
//        }
//    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getClassDetail(classId: String): Flow<ResultWrapper<ClassData>> {
        return callbackFlow {
            classRef.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach {
                        val classDataIterator = it.getValue(ClassData::class.java)
                        if (classDataIterator?.id == classId) {
                            trySendBlocking(ResultWrapper.success(classDataIterator))
                            cancel()
                            return@forEach
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })
            awaitClose {
            }
        }
    }

    override suspend fun getAttendanceByLessonId(lessonId: String): Flow<ResultWrapper<List<StudentLesson>>> {
        return callbackFlow {
            val studentLessonList = mutableListOf<StudentLesson>()
            studentLessonRef.child(lessonId)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        studentLessonList.clear()
                        snapshot.children.forEach {
                            val studentLesson = it.getValue(StudentLesson::class.java)
                            if (studentLesson != null) {
                                studentLessonList.add(studentLesson)
                            }
                        }
                        trySendBlocking(ResultWrapper.success(studentLessonList))
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }

                })
            awaitClose {

            }
        }
    }

    override suspend fun getMemberByClassId(classId: String): Flow<ResultWrapper<List<User>>> {
        return callbackFlow {
            val userClassList = mutableListOf<UserClass>()

            userClassRef.orderByChild("classId").equalTo(classId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userClassList.clear()
                        snapshot.children.forEach { uc ->
                            val userClass = uc.getValue(UserClass::class.java)
                            if (userClass != null) {
                                userClassList.add(userClass)
                            }

//                            //getUserById
//                            userRef.child(userClass?.userId.toString()).get()
//                                .addOnCompleteListener { u ->
//                                    val user = u.result.getValue(User::class.java)
//                                    if (user != null) {
//                                        memberList.add(user)
//                                    }
//                                    memberAdapter.notifyDataSetChanged()
//                                }
                        }

                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                }
                )
        }
    }

    override suspend fun getMemberCountByClassId(classId: String): Flow<ResultWrapper<Int>> {
        return callbackFlow {
            val userClassList = mutableListOf<UserClass>()
            userClassRef.orderByChild("classId").equalTo(classId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userClassList.clear()
                        snapshot.children.forEach { uc ->
                            val userClass = uc.getValue(UserClass::class.java)
                            if (userClass != null) {
                                userClassList.add(userClass)
                            }
                        }
                        trySendBlocking(ResultWrapper.success(userClassList.size))
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                }
                )

            awaitClose { }
        }
    }

    override suspend fun sendFeedBack(lessonFeedBack: LessonFeedBack): Flow<ResultWrapper<Boolean>> {
        return callbackFlow {
            feedbackLessonRef.child(lessonFeedBack.lessonId).push()
                .setValue(lessonFeedBack).addOnCompleteListener {
                    if (it.isSuccessful) {
                        trySendBlocking(ResultWrapper.success(true))
                    }
                }

            awaitClose { }
        }
    }

    override fun getLessonFeedBack(lessonId: String): Flow<ResultWrapper<List<LessonFeedBack>>> {
        return callbackFlow {
            val lessonFeedBackList = mutableListOf<LessonFeedBack>()
            feedbackLessonRef.child(lessonId).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    lessonFeedBackList.clear()
                    snapshot.children.forEach {
                        val lessonFeedBack = it.getValue(LessonFeedBack::class.java)
                        if (lessonFeedBack != null) {
                            lessonFeedBackList.add(lessonFeedBack)
                        }
                    }
                    trySendBlocking(ResultWrapper.success(lessonFeedBackList))
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

            awaitClose {}
        }
    }

    override fun createPost(postData: Post): Flow<ResultWrapper<Boolean>> {
        return callbackFlow {
            postData.id = postRef.push().key ?: ""
            postRef.child(postData.classId).child(postData.id).setValue(postData)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        trySendBlocking(ResultWrapper.success(true))
                    }
                }

            awaitClose {}
        }
    }

    override fun addLike(postId: String): Flow<ResultWrapper<Boolean>> {
        return callbackFlow<ResultWrapper<Boolean>> {
            val likeData = LikeData(postId, userId = currentUserId.toString())
            likeRef.child(postId).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.hasChild(currentUserId.toString())) {
                        likeRef.child(postId).child(currentUserId.toString()).removeValue()
                            .addOnCompleteListener {
                                trySendBlocking(ResultWrapper.success(false))
                            }
                    } else {
                        likeRef.child(postId).child(currentUserId.toString()).setValue(likeData)
                            .addOnCompleteListener {
                                trySendBlocking(ResultWrapper.success(true))
                            }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            })



            awaitClose { }
        }.backgroundSub()
    }

    override fun addComment(postId: String, content: String): Flow<ResultWrapper<Boolean>> {
        return callbackFlow<ResultWrapper<Boolean>> {
            commentRef.push().setValue(
                CommentData(
                    postId = postId, userId = currentUserId ?: "", content = content
                )
            ).addOnCompleteListener {
                trySendBlocking(ResultWrapper.success(true))
            }
            awaitClose { }
        }.backgroundSub()
    }

    override fun addContribution(
        userId: String,
        classId: String,
        type: Long,
        point: Long
    ): Flow<ResultWrapper<Boolean>> {
        return callbackFlow<ResultWrapper<Boolean>> {
            var uid = userId
            if (userId.isEmpty()) uid = currentUserId.toString()
            val contribution = Contribution(Utils.createId(), uid, classId, type, point)
            contributionRef.child("$classId/$uid").push().setValue(contribution)
                .addOnSuccessListener {
                    cancel()
                }
            awaitClose {}
        }.backgroundSub()
    }

    override fun getAllContributionByClassId(classId: String): Flow<ResultWrapper<List<UserContribution>>> {
        return callbackFlow<ResultWrapper<List<UserContribution>>> {
            contributionRef.child(classId)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {

                        /* structure is
                            Contribution
                            |--ClassId
                            |    |---UserId
                            |    |      |--ContributionId
                                            |--ContributionData
                         */

                        val userContributionList = mutableListOf<UserContribution>()
                        snapshot.children.forEach { it ->
                            val userId = it.key.toString()
                            val userNode = it.value as java.util.HashMap<*, *>
                            val contributionList = mutableListOf<Contribution>()
                            userNode.forEach { userNodeData ->
                                val contribution = Contribution(
                                    id = ((userNodeData.value as java.util.HashMap<*, *>)["id"]
                                        ?: "") as String,
                                    classId = ((userNodeData.value as java.util.HashMap<*, *>)["classId"]
                                        ?: "") as String,
                                    userId = ((userNodeData.value as java.util.HashMap<*, *>)["userId"]
                                        ?: "") as String,
                                    type = (((userNodeData.value as java.util.HashMap<*, *>)["type"]
                                        ?: 0L) as Long),
                                    point = ((userNodeData.value as java.util.HashMap<*, *>)["point"]
                                        ?: 0L) as Long,
                                )
                                contributionList.add(contribution)
                            }
                            userContributionList.add(UserContribution(userId, contributionList))
                        }

                        trySendBlocking(ResultWrapper.success(userContributionList))
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })

            awaitClose {}
        }.backgroundSub()
    }

    override fun getContributionByUserIdAndClassId(
        userId: String,
        classId: String
    ): Flow<ResultWrapper<List<UserContribution>>> {
        return callbackFlow<ResultWrapper<List<UserContribution>>> {
            awaitClose {}
        }.backgroundSub()
    }

    override fun getAllContributionByUserId(
        userId: String,
    ): Flow<ResultWrapper<List<Contribution>>> {
        return callbackFlow<ResultWrapper<List<Contribution>>> {
            contributionRef.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val contributionList = mutableListOf<Contribution>()
                    /* structure is
                        Contribution
                        |--ClassId
                        |    |---UserId
                        |    |      |--ContributionData
                     */

                    snapshot.children.forEach { classId ->
                        val uid = classId.value as java.util.HashMap<*, *>
                        if (uid[userId] != null)
                            (uid[userId] as java.util.HashMap<*, *>).forEach {
                                val contribution = Contribution(
                                    id = ((it.value as java.util.HashMap<*, *>)["id"]
                                        ?: "") as String,
                                    classId = ((it.value as java.util.HashMap<*, *>)["classId"]
                                        ?: "") as String,
                                    userId = ((it.value as java.util.HashMap<*, *>)["userId"]
                                        ?: "") as String,
                                    type = (((it.value as java.util.HashMap<*, *>)["type"]
                                        ?: 0L) as Long),
                                    point = ((it.value as java.util.HashMap<*, *>)["point"]
                                        ?: 0L) as Long,
                                )
                                contributionList.add(contribution)
                            }
                    }

                    trySendBlocking(ResultWrapper.success(contributionList))
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

            awaitClose {}
        }.backgroundSub()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun submitWork(
        classId: String,
        assignmentId: String,
        fileUrl: String?,
        fileName: String,
        dueDate: Long
    ): Flow<ResultWrapper<Boolean>> {
        return callbackFlow {
//            val key =
//                submissionRef.child(classId).child(assignmentId).child(currentUserId.toString())
//                    .push().key
            val nowInMilisecond = Calendar.getInstance().timeInMillis
            val isLate = dueDate < nowInMilisecond
            submissionRef.child(classId).child(assignmentId).child(currentUserId.toString())
//                .child(key.toString())
                .setValue(
                    Submission(
                        Utils.createId(),
                        classId,
                        assignmentId,
                        fileUrl.toString(),
                        currentUserId.toString(),
                        nowInMilisecond,
                        isLate,
                        fileName,
                        0
                    )
                ).addOnCompleteListener {
                    trySendBlocking(ResultWrapper.success(true))
                }

            awaitClose { }
        }.backgroundSub()
    }

    override fun getSubmissionByUserId(
        classId: String,
        assignmentId: String,
        userId: String
    ): Flow<ResultWrapper<Submission?>> {
        return callbackFlow {

            submissionRef.child(classId).child(assignmentId).child(currentUserId.toString())
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val submission = snapshot.getValue(Submission::class.java)
                        if (submission != null) {
                            trySendBlocking(ResultWrapper.success(submission))
                        } else {
                            trySendBlocking(ResultWrapper.success(null))
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })

            awaitClose { }
        }.backgroundSub()
    }

    override fun cancelSubmittedWork(
        classId: String,
        assignmentId: String
    ): Flow<ResultWrapper<Boolean>> {
        return callbackFlow {

            submissionRef.child(classId).child(assignmentId).child(currentUserId.toString())
                .removeValue().addOnCompleteListener {
                    trySendBlocking(ResultWrapper.success(true))
                }.addOnFailureListener {
                    trySendBlocking(ResultWrapper.success(false))
                }

            awaitClose { }
        }.backgroundSub()
    }

    override fun getAllSubmission(
        classId: String,
        assignmentId: String
    ): Flow<ResultWrapper<List<Submission>>> {
        return callbackFlow {

            val submissionList = mutableListOf<Submission>()

            submissionRef.child(classId).child(assignmentId)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        submissionList.clear()
                        snapshot.children.forEach {
                            val submission = it.getValue(Submission::class.java)
                            if (submission != null) {
                                submissionList.add(submission)
                            }
                        }
                        trySendBlocking(ResultWrapper.success(submissionList))
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })

            awaitClose { }
        }.backgroundSub()
    }

    override suspend fun sendNote(
        assignmentId: String,
        userId: String,
        note: String
    ): Flow<ResultWrapper<Note>> {
        return callbackFlow {
            val noteD = Note(
                Utils.createId(),
                assignmentId,
                userId,
                Calendar.getInstance().timeInMillis,
                note.trim(),
                currentUserId.toString(),
            )

            noteRef.child(assignmentId).child(userId)
//                .child(key.toString())
                .setValue(
                    noteD
                ).addOnCompleteListener {
                    trySendBlocking(ResultWrapper.success(noteD))
                }

            awaitClose { }
        }.backgroundSub()
    }

    override suspend fun sendMark(
        assignmentId: String,
        userId: String,
        mark: Double
    ): Flow<ResultWrapper<Mark>> {
        return callbackFlow {
            val markSend = Mark(
                Utils.createId(),
                assignmentId,
                userId,
                Calendar.getInstance().timeInMillis,
                mark = mark,
                currentUserId.toString(),
            )

            markRef.child(assignmentId).child(userId)
//                .child(key.toString())
                .setValue(
                    markSend
                ).addOnCompleteListener {
                    trySendBlocking(ResultWrapper.success(markSend))
                }

            awaitClose { }
        }.backgroundSub()
    }

    override fun getMarkByUserId(assignmentId: String): Flow<ResultWrapper<Mark>> {
       return callbackFlow {

        markRef.child(assignmentId).child(currentUserId.toString())
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val mark = snapshot.getValue(Mark::class.java)
                    if(mark != null){
                       trySendBlocking(ResultWrapper.success(mark))
                    }else{
                        trySendBlocking(ResultWrapper.error<Mark>(""))
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        awaitClose { }
    }.backgroundSub()
    }

    override fun getNoteByUserId(assignmentId: String): Flow<ResultWrapper<Note>> {
        return callbackFlow {

            noteRef.child(assignmentId).child(currentUserId.toString())
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val note = snapshot.getValue(Note::class.java)
                        if(note != null){
                            trySendBlocking(ResultWrapper.success(note))
                        }else{
                            trySendBlocking(ResultWrapper.error<Note>(""))
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }

                })

            awaitClose { }
        }.backgroundSub()
    }
}