package com.example.pclassmanagement.data.model

data class ActionWrapper(
    val name: String = "",
    val payload: Any? = null,
)