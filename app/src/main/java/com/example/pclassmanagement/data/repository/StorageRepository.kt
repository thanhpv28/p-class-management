package com.example.pclassmanagement.data.repository

import android.net.Uri
import com.example.pclassmanagement.data.api.ResultWrapper
import kotlinx.coroutines.flow.Flow

interface StorageRepository {
    suspend fun uploadPostImage(postId: String,uri: Uri) : Flow<ResultWrapper<String>>

    suspend fun uploadAvatarImage(uri: Uri) : Flow<ResultWrapper<String>>

    suspend fun uploadABackgroundImage(uri: Uri) : Flow<ResultWrapper<String>>

    suspend fun uploadSubmission(uri: Uri,classId: String, assignmentId: String) : Flow<ResultWrapper<String>>
}