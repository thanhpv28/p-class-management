package com.example.pclassmanagement.data.model

data class StudentLesson(
    val attendDate: String,
    val attended: Boolean,
    val late: Boolean,
    val lessonId: String,
    val userId: String
): java.io.Serializable{
    constructor(): this("", false, false, "","")
}