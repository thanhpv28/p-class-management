package com.example.pclassmanagement.data.model

data class Lesson(
    var classId: String,
    var detail: String,
    var id: String,
    var finished: Boolean,
    var inAttendance: Boolean,
    var started: Boolean,
    var startDate: String,
    var finishedDate: String,
    var teacherId: String,
    var title: String
):java.io.Serializable {
    constructor() : this("", "", "", false, false, false, "","", "", "")
}