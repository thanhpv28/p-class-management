package com.example.pclassmanagement.data.model

data class User(
    val email: String,
    val fullname: String,
    val id: String,
    val imageUrl: String,
    val background: String,
    val password: String,
    val username: String,
    val studentCode: String,
    val studentClass: String,
    val naturalId: String,
    val phoneNumber: String,
    val gender: Int,
    val address: String,
) {
    constructor() : this(
        "", "", "", "", "", "", "", "", "", "", "", 0, ""
    )
}