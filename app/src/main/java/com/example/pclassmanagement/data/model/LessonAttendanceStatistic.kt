package com.example.pclassmanagement.data.model

data class LessonAttendanceStatistic(
    var totalMember: Int = 0,
    var attended: Int = 0,
    var inTime: Int = 0,
    var late: Int = 0,
    var absent: Int = 0
)
