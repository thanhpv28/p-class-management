package com.example.pclassmanagement.data.model

data class LessonFeedBack(
    val userId: String,
    val lessonId: String,
    val rating: Float,
    val message: String
): java.io.Serializable{
    constructor(): this("","",0f,"")
}
