package com.example.pclassmanagement.data.model

data class MarkResponse(
    val message: String,
    val score: Double,
    val statusCode: Int
)