package com.example.pclassmanagement.data.local

import androidx.core.content.edit
import com.example.pclassmanagement.data.SharedPreferencesKey
import com.example.pclassmanagement.data.model.UserInfoSaved

interface LocalDataAccess {
    fun saveInfo(userId: String, email: String, password: String, remember: Boolean)

    fun getUserId() : String
    fun getEmail() : String
    fun getPassword() : String
    fun getRemember() : Boolean
    fun getUserInfo(): UserInfoSaved
    fun clear()
}