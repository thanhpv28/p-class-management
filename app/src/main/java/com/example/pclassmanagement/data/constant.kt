package com.example.pclassmanagement.data

import android.util.Log

const val ACTION_CLASS_PICK = "ACTION_CLASS_PICK"

fun log(message: String,tag: String = "TAG" ){
    Log.d("$tag...", message)
}

object Action {
    object Item {
        const val PICK_CLASS = "PICK_CLASS"
        const val LIKE_POST = "LIKE_POST"
        const val COMMENT_POST = "COMMENT_POST"
        const val ITEM_CHECK = "ITEM_CHECK"
        const val ITEM_UNCHECK = "ITEM_UNCHECK"
        const val SHOW_TOAST = "SHOW_TOAST"
        const val LESSON_VIEW_STATISTIC = "LESSON_VIEW_STATISTIC"
        const val LESSON_FEEDBACK = "LESSON_FEEDBACK"
        const val MESSAGE = "MESSAGE"
        const val VIEW_PROFILE = "VIEW_PROFILE"
        const val ATTENDANCE_IN_TIME = "ATTENDANCE_IN_TIME"
        const val ATTENDANCE_LATE = "ATTENDANCE_LATE"
        const val ATTENDANCE_MISS = "ATTENDANCE_MISS"
    }
}

object FragmentChatARGS{
    object KEY{
        const val USER = "USER"
        const val USER_ID = "USER_ID"
        const val CHAT_ID = "CHAT_ID"
        const val IS_SHOW_MESSAGE_DETAIL = "IS_SHOW_MESSAGE_DETAIL"
    }
}

object FragmentLandingPageARGS{
    object KEY{
        const val CLASS_ID = "CLASS_ID"
        const val OWNER_ID = "OWNER_ID"
    }
}

object FragmentHomeARGS{
    object KEY{
        const val POST_ID = "POST_ID"
        const val POST = "POST"
    }
}

object FragmentAssignmentARGS{
    object KEY{
        const val ASSIGNMENT = "ASSIGNMENT"
    }
}

object FragmentProfileARGS{
    object KEY{
        const val USER_ID = "USER_ID"
    }
}

object FragmentLessonARGS{
    object KEY{
        const val LESSON_ID = "LESSON_ID"
        const val CLASS_ID = "CLASS_ID"
        const val TEACHER_ID = "TEACHER_ID"
        const val LESSON = "LESSON"
    }
}

object Table{
    const val USER_CLASS = "UserClass"
    const val USER = "User"
    const val CLASS = "Class"
    const val POST = "Post"
    const val CONTRIBUTION = "Contribution"
    const val COMMENT = "Comment"
    const val LIKE = "Like"
    const val ASSIGNMENT = "Assignment"
    const val LESSON = "Lesson"
    const val STUDENT_LESSON = "StudentLesson"
    const val FEEDBACK_LESSON = "FeedbackLesson"
    const val ASSIGNMENT_USER = "AssignmentUser"
    const val CHAT = "Chat"
    const val MESSAGE = "Message"
    const val USER_AVATAR = "User_Avatar"
    const val USER_BACKGROUND = "User_Background"
    const val Submission = "Submission"
    const val NOTE = "Note"
    const val MARK = "Mark"
    const val RESULT = "Result"
    const val MARK_TYPE = "MarkType"
}

object SharedPreferencesKey{
    const val USER_ID = "USER_ID"
    const val EMAIL = "EMAIL"
    const val PASSWORD = "PASSWORD"
    const val REMEMBER_INFO = "REMEMBER_INFO"
}

object Gender{
    const val MALE = 0
    const val FEMALE = 1
    const val OTHER = 2
}

object ContributionType{
    const val LIKE = 1L
    const val COMMENT = 2L
    const val CREATE_POST = 2L
    const val FEEDBACK = 3L
    const val IN_TIME_ATTENDANCE = 4L
    const val LATE_ATTENDANCE = 5L
    const val MISS_ATTENDANCE = 6L
    const val UNLIKE = 7L
}

object ContributionPoint{
    const val LIKE = +1L
    const val COMMENT = +2L
    const val FEEDBACK = +5L
    const val CREATE_POST = +5L
    const val IN_TIME_ATTENDANCE = +3L
    const val LATE_ATTENDANCE = -4L
    const val MISS_ATTENDANCE = -5L
    const val UNLIKE = -1L
}

//object Table{
//    const val USER_CLASS = "UserClass"
//    const val USER = "User"
//    const val CLASS = "Class"
//}