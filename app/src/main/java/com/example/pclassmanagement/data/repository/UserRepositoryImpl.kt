package com.example.pclassmanagement.data.repository

import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.User
import com.example.pclassmanagement.shared.ext.backgroundSub
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor() : UserRepository {

    private val userRef = FirebaseDatabase.getInstance().getReference(Table.USER)
    private val mAuth = FirebaseAuth.getInstance()

    override suspend fun signInWithEmailAndPassword(
        email: String, password: String
    ): Flow<ResultWrapper<AuthResult>> = flow<ResultWrapper<AuthResult>> {

        val task = mAuth.signInWithEmailAndPassword(email, password)

        try {
            Tasks.await(task)
            emit(ResultWrapper.success(task.result))
        } catch (e: java.lang.Exception) {
            emit(ResultWrapper.error(message = task.exception?.message.toString()))
        }
    }.backgroundSub()

    override suspend fun register(
        username: String,
        email: String,
        password: String
    ): Flow<ResultWrapper<AuthResult>> = flow<ResultWrapper<AuthResult>> {
        val task = mAuth.createUserWithEmailAndPassword(email, password)
        try {
            Tasks.await(task)
            emit(ResultWrapper.success(task.result))
        } catch (e: java.lang.Exception) {
            emit(ResultWrapper.error(message = task.exception?.message.toString()))
        }
    }.backgroundSub()

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getUserInfo(userId: String): Flow<ResultWrapper<User>> {
        return callbackFlow {
            userRef.child(userId).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = snapshot.getValue(User::class.java)
                    trySendBlocking(ResultWrapper.success(user!!))
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })

            awaitClose {  }
        }
    }

    override suspend fun updateUserInfo(

        value: HashMap<String, Any>
    ): Flow<ResultWrapper<Boolean>> {
        return callbackFlow {
            userRef.child(mAuth.uid.toString()).updateChildren(value).addOnSuccessListener {
                trySendBlocking(ResultWrapper.success(true))
            }
            awaitClose {  }
        }
    }

}