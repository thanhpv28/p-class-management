package com.example.pclassmanagement.data.model

data class UserInfoSaved(
    val userId: String = "",
    val email: String = "",
    val password: String = "",
    val remember: Boolean = false
)
