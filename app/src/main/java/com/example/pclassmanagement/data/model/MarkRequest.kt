package com.example.pclassmanagement.data.model

data class MarkRequest(
    val id: Int,
    val ans: String,
)