package com.example.pclassmanagement.data.model

data class ClassData(
    val id: String,
    val name: String,
    val des: String,
    val ownerId: String,
    val invitationCode: String,
    val createdDate: String,
    val schedule: String,
    val thumb: String
){
    constructor(): this("","","","","","","","")
}
