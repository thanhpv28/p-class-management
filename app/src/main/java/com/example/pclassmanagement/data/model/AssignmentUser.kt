package com.example.pclassmanagement.data.model

data class AssignmentUser(
    val id: String,
    val userId: String,
    val assignmentId: String,
    val submitDate: String,
    val status: Int
): java.io.Serializable {
    constructor() : this("","","","",0)
}
