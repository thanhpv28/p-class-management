package com.example.pclassmanagement.data.model

import java.io.Serializable

data class Post(
    var id: String,
    val ownerId: String,
    val classId: String,
    val type: Long,
    var thumb: String,
    val content: String,
    val createdDate: Long
): Serializable{

    constructor() : this("","","",0,"","",0)

}