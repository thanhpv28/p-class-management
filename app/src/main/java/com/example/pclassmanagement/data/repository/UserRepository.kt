package com.example.pclassmanagement.data.repository

import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.User
import com.google.firebase.auth.AuthResult
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    suspend fun signInWithEmailAndPassword(
        email: String,
        password: String
    ): Flow<ResultWrapper<AuthResult>>

    suspend fun register(username: String, email: String, password: String): Flow<ResultWrapper<AuthResult>>

    suspend fun getUserInfo(userId: String): Flow<ResultWrapper<User>>

    suspend fun updateUserInfo(value: HashMap<String, Any>): Flow<ResultWrapper<Boolean>>
    
}
