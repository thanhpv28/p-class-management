package com.example.pclassmanagement.data.api

import androidx.annotation.Keep
import com.google.gson.Gson
import retrofit2.HttpException

@Keep
data class ResultWrapper<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val throwable: Throwable? = null,
    val error: ErrorResp? = null,
    val code: Int = 0
) {

    sealed class Status {
        object SUCCESS : Status()
        object ERROR : Status()
        object LOADING : Status()
    }

    companion object {
        fun <T> success(data: T): ResultWrapper<T> {
            return ResultWrapper(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String): ResultWrapper<T> {
            return ResultWrapper(Status.ERROR, null, message)
        }

        fun <T> error(message: String, error: ErrorResp, code: Int = 0): ResultWrapper<T> {
            return ResultWrapper(Status.ERROR, null, message, null, error, code)
        }

        fun <T> error(cause: Throwable): ResultWrapper<T> {
            if (cause is HttpException) {
                try {
                    val gson = Gson()
                    val errorResponse: DefaultResponse<*>? = gson.fromJson(
                        cause.response()?.errorBody()?.charStream(),
                        DefaultResponse::class.java
                    )

                    return ResultWrapper(
                        Status.ERROR,
                        null,
                        errorResponse?.error_code,
                        throwable = cause
                    )
                } catch (_: Exception) {
                }
            }
            return ResultWrapper(Status.ERROR, null, null, throwable = cause)
        }

        fun <T> loading(data: T? = null): ResultWrapper<T> {
            return ResultWrapper(Status.LOADING, data, null)
        }
    }
}

@Keep
class DefaultResponse<out T>(
    val success: Boolean,
    val error_message: String?,
    val error_code: String?,
    val data: T?,
    val total: Any?,
)