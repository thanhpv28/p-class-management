package com.example.pclassmanagement.data.model

data class FeedbackData(
    val rating1: Float = 0f,
    val rating2: Float = 0f,
    val rating3: Float = 0f,
    val rating4: Float = 0f,
    val rating5: Float = 0f,
    val ratingAverage: Float = 0f,
    val feedbackList: List<LessonFeedBack>
)
