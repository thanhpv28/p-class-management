package com.example.pclassmanagement.data.model

data class UserClass(
    val classId: String,
    val userId: String
){
    constructor(): this("","")
}