package com.example.pclassmanagement.data.api

import androidx.annotation.Keep

@Keep
data class ErrorResp(
  val success: Boolean,
  val error_message: Any?,
  val error_code: String?,
  val data: Any? = null,
  val total: Any?,
)