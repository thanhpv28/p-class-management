package com.example.pclassmanagement.data.repository

import android.net.Uri
import com.example.pclassmanagement.data.Table
import com.example.pclassmanagement.data.api.ResultWrapper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class StorageRepositoryImpl @Inject constructor() : StorageRepository {

    private val storageRef = FirebaseStorage.getInstance()
    private val firebaseUser = FirebaseAuth.getInstance()
    private val postRef = storageRef.getReference(Table.POST)
    private val userAvatar = storageRef.getReference(Table.USER_AVATAR)
    private val userBackground = storageRef.getReference(Table.USER_BACKGROUND)
    private val submissionRef = storageRef.getReference(Table.Submission)

    override suspend fun uploadPostImage(postId: String, uri: Uri): Flow<ResultWrapper<String>> {
        return callbackFlow {

            val fileRef = postRef.child(postId).child(firebaseUser.uid.toString())
            fileRef.putFile(uri).addOnCompleteListener {
                if (it.isSuccessful) {
                    fileRef.downloadUrl.addOnCompleteListener { fileStatus ->
                        if (fileStatus.isSuccessful) {
                            trySendBlocking(ResultWrapper.success(fileStatus.result.toString()))
                        }
                    }


                }

            }
            awaitClose {

            }
        }
    }

    override suspend fun uploadAvatarImage(uri: Uri): Flow<ResultWrapper<String>> {
        return callbackFlow {
            userAvatar.child(firebaseUser.uid.toString()).putFile(uri).addOnCompleteListener {
                if (it.isSuccessful) {
                    userAvatar.child(firebaseUser.uid.toString())
                        .downloadUrl.addOnCompleteListener { fileStatus ->
                            if (fileStatus.isSuccessful) {
                                trySendBlocking(ResultWrapper.success(fileStatus.result.toString()))
                            }
                        }


                }

            }


            awaitClose {}
        }
    }

    override suspend fun uploadABackgroundImage(uri: Uri): Flow<ResultWrapper<String>> {
        return callbackFlow {
            userBackground.child(firebaseUser.uid.toString()).putFile(uri).addOnCompleteListener {
                if (it.isSuccessful) {
                    userBackground.child(firebaseUser.uid.toString())
                        .downloadUrl.addOnCompleteListener { fileStatus ->
                            if (fileStatus.isSuccessful) {
                                trySendBlocking(ResultWrapper.success(fileStatus.result.toString()))
                            }
                        }


                }

            }


            awaitClose {}
        }
    }

    override suspend fun uploadSubmission(uri: Uri, classId: String, assignmentId: String): Flow<ResultWrapper<String>> {
        return callbackFlow {
            submissionRef.child(classId).child(assignmentId)
                .child(firebaseUser.uid.toString()).putFile(uri).addOnCompleteListener {
                if (it.isSuccessful) {
                    submissionRef.child(classId).child(assignmentId)
                        .child(firebaseUser.uid.toString())
                        .downloadUrl.addOnCompleteListener { fileStatus ->
                            if (fileStatus.isSuccessful) {
                                trySendBlocking(ResultWrapper.success(fileStatus.result.toString()))
                            }
                        }


                }

            }


            awaitClose {}
        }
    }
}