package com.example.pclassmanagement.data.repository

import com.example.pclassmanagement.data.api.ResultWrapper
import com.example.pclassmanagement.data.model.*
import kotlinx.coroutines.flow.Flow

interface ContentRepository {

    suspend fun getClassByCode(classCode: String): Flow<ResultWrapper<ClassData>>

    suspend fun joinClass(userId: String, classId: String): Flow<ResultWrapper<Boolean>>

    suspend fun createClass(className: String, classDes: String): Flow<ResultWrapper<UserClass>>

    suspend fun getAllClassByUserId(userId: String): Flow<ResultWrapper<List<String>>>

//    suspend fun getAllClassDetailByUserId(userId: String): Flow<ResultWrapper<List<ClassData>>>

    suspend fun getClassDetail(classId: String): Flow<ResultWrapper<ClassData>>

    suspend fun getAttendanceByLessonId(lessonId: String): Flow<ResultWrapper<List<StudentLesson>>>

    suspend fun getMemberByClassId(classId: String): Flow<ResultWrapper<List<User>>>

    suspend fun getMemberCountByClassId(classId: String): Flow<ResultWrapper<Int>>

    suspend fun sendFeedBack(lessonFeedBack: LessonFeedBack): Flow<ResultWrapper<Boolean>>

    fun getLessonFeedBack(lessonId: String): Flow<ResultWrapper<List<LessonFeedBack>>>

    // class home
    fun createPost(postData: Post): Flow<ResultWrapper<Boolean>>

    fun addLike(postId: String): Flow<ResultWrapper<Boolean>>

    fun addComment(postId: String, content: String): Flow<ResultWrapper<Boolean>>

    fun addContribution(
        userId: String = "", classId: String, type: Long, point: Long
    ): Flow<ResultWrapper<Boolean>>

    fun getAllContributionByClassId(classId: String): Flow<ResultWrapper<List<UserContribution>>>

    fun getContributionByUserIdAndClassId(
        userId: String,
        classId: String
    ): Flow<ResultWrapper<List<UserContribution>>>

    fun getAllContributionByUserId(
        userId: String,
    ): Flow<ResultWrapper<List<Contribution>>>

    fun submitWork(
        classId: String,
        assignmentId: String,
        fileUrl: String?,
        fileName: String,
        dueDate: Long,
    ): Flow<ResultWrapper<Boolean>>

    fun getSubmissionByUserId(
        classId: String,
        assignmentId: String,
        userId: String = ""
    ): Flow<ResultWrapper<Submission?>>

    fun cancelSubmittedWork(classId: String, assignmentId: String): Flow<ResultWrapper<Boolean>>

    fun getAllSubmission(classId: String, assignmentId: String): Flow<ResultWrapper<List<Submission>>>

    suspend fun sendNote(assignmentId: String, userId: String, note: String): Flow<ResultWrapper<Note>>

    suspend fun sendMark(assignmentId: String, userId: String, mark: Double): Flow<ResultWrapper<Mark>>

    fun getMarkByUserId(assignmentId: String): Flow<ResultWrapper<Mark>>

    fun getNoteByUserId(assignmentId: String): Flow<ResultWrapper<Note>>

}
