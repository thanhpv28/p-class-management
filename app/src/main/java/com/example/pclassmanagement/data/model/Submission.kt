package com.example.pclassmanagement.data.model

data class Submission(
    val id: String,
    val classId: String,
    val assignmentId: String,
    val fileUrl: String,
    val userId: String,
    val createdDate: Long,
    val late: Boolean,
    val fileName: String,
    val fileType: Int
): java.io.Serializable{
    constructor(): this("","","","","",0,false,"",0)
}
