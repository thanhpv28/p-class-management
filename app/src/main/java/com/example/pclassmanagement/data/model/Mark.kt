package com.example.pclassmanagement.data.model

data class Mark(
    val id: String,
    val assignmentId: String,
    val userId: String,
    val createdDate: Long,
    val mark: Double,
    val createBy: String
): java.io.Serializable{
    constructor(): this("","","",0,0.0,"")
}
