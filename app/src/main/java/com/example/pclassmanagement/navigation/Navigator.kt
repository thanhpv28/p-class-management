package com.example.pclassmanagement.navigation

import android.app.Activity
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.pclassmanagement.R

object Navigator {

    fun Fragment.requestNavigate(@IdRes id: Int, args: Bundle? = null) {
        findNavController().navigate(id, args)
    }

    fun Activity.requestNavigate(@IdRes id: Int, args: Bundle? = null) {
        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(id, args)
    }

    fun Fragment.popBack() {
        findNavController().popBackStack()
    }

    fun Fragment.popTo(@IdRes id: Int, inclusive: Boolean = false): Boolean {
        return findNavController().popBackStack(id, inclusive)
    }
}

