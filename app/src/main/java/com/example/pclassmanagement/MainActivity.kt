package com.example.pclassmanagement

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.pclassmanagement.databinding.ActivityMainBinding
import com.example.pclassmanagement.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }
}